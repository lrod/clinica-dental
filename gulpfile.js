var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass('app.scss')
        .copy(
            [
                'bower_components/jquery/dist/jquery.js',
                'bower_components/jquery-ui/jquery-ui.js',
                //'bower_components/jquery-validation/dist/jquery.validate.min.js',
                'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
                'bower_components/bootstrap-calendar/js/calendar.js',
                'bower_components/underscore/underscore.js',
                'bower_components/moment/moment.js',
                'bower_components/fullcalendar/dist/fullcalendar.js',
                'bower_components/fullcalendar/dist/locale/es.js'

            ],
            'resources/assets/js/vendor'
        )
        .copy(
            [
                'bower_components/bootstrap-calendar/css/calendar.css',
                'bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss',
                'bower_components/jquery-ui/themes/cupertino/jquery-ui.min.css',
                'bower_components/fullcalendar/dist/fullcalendar.css'
            ],
            'resources/assets/css/vendor'
        )
        .styles(
            [
                'resources/assets/css/vendor/calendar.css',
                'resources/assets/css/vendor/_bootstrap.scss',
                'resources/assets/css/vendor/jquery-ui.min.css',
                'resources/assets/css/vendor/datepicker.css',
                'resources/assets/css/vendor//fullcalendar.css'
                
            ],
            'public/css/app.css'
        )
        .scripts(
            [
                'resources/assets/js/vendor/jquery.js',
                'resources/assets/js/vendor/jquery-ui.js',
                //'resources/assets/js/vendor/jquery.validate.min.js',
                'resources/assets/js/vendor/bootstrap.js',
                'resources/assets/js/vendor/underscore.js',
                'resources/assets/js/vendor/calendar.js',
                'resources/assets/js/vendor/moment.js',
                'resources/assets/js/vendor/bootstrap-datepicker.js',
                'resources/assets/js/vendor/fullcalendar.js',
                'resources/assets/js/vendor/es.js',                
                'app.js',
            ],
            'public/js/app.js'
        )
        .version(
            [
                "public/css/app.css",
                "public/js/app.js"
            ]
        );


    //mix.sass('app.scss').version('css/app.css')
    //    .styles(['app.css'], 'public/css/app.css'
     //   ).scripts(['app.js'], 'public/js/app.js');
//mix.phpUnit();
});
