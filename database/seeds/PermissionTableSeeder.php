<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'name' => 'role-list',
                'display_name' => 'Ver listado de roles',
                'description' => 'Muestra unicamente el listado de roles'
            ],
            [
                'name' => 'role-create',
                'display_name' => 'Crear Rol',
                'description' => 'Crear Nuevo Rol'
            ],
            [
                'name' => 'role-edit',
                'display_name' => 'Editar Rol',
                'description' => 'Editar Rol'
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Elimianr Rol',
                'description' => 'Eliminar Rol'
            ],
            [
                'name' => 'paciente-list',
                'display_name' => 'Ver listado de pacientes',
                'description' => 'Muestra unicamente el listado de pacientes'
            ],
            [
                'name' => 'paciente-create',
                'display_name' => 'Crear Paciente',
                'description' => 'Crear Nuevo Paciente'
            ],
            [
                'name' => 'paciente-edit',
                'display_name' => 'Editar Paciente',
                'description' => 'Editar Paciente'
            ],
            [
                'name' => 'paciente-delete',
                'display_name' => 'Eliminar Paciente',
                'description' => 'Eliminar Paciente'
            ],
            [
                'name' => 'medico-list',
                'display_name' => 'Ver listado de medicos',
                'description' => 'Muestra unicamente el listado de medicos'
             ],
            [
                'name' => 'medico-create',
                'display_name' => 'Crear Medico',
                'description' => 'Crear Nuevo Medico'
            ],
            [
                'name' => 'medico-edit',
                'display_name' => 'Editar Medico',
                'description' => 'Editar Medico'
            ],
            [
                'name' => 'medico-delete',
                'display_name' => 'Eliminar Medico',
                'description' => 'Eliminar Medico'
            ],
            [
                'name' => 'medico-enable',
                'display_name' => 'Habilitar Medico',
                'description' => 'Habilitar/Deshabilitar Medico'
            ],
            [
                'name' => 'cita-list',
                'display_name' => 'Ver listado de citas',
                'description' => 'Muestra unicamente el listado de citas'
            ],
            [
                'name' => 'cita-create',
                'display_name' => 'Crear Cita',
                'description' => 'Crear Nueva Cita'
            ],
            [
                'name' => 'cita-edit',
                'display_name' => 'Editar Cita',
                'description' => 'Editar Cita'
            ],
            [
                'name' => 'cita-delete',
                'display_name' => 'Eliminar Cita',
                'description' => 'Eliminar Cita'
            ],
            [
                'name' => 'sevivicio-list',
                'display_name' => 'Ver listado de servicios',
                'description' => 'Muestra unicamente el listado de Servicios'
            ],
            [
                'name' => 'servicio-create',
                'display_name' => 'Crear Servicio',
                'description' => 'Crear Nueva Servicio'
            ],
            [
                'name' => 'servicio-edit',
                'display_name' => 'Editar Servicio',
                'description' => 'Editar Servicio'
            ],
            [
                'name' => 'servicio-delete',
                'display_name' => 'Eliminar Servicio',
                'description' => 'Eliminar Servicio'
            ],
            [
                'name' => 'procedimiento-list',
                'display_name' => 'Ver listado de procedimientos',
                'description' => 'Muestra unicamente el listado de procedimientos'
            ],
            [
                'name' => 'procedimiento-create',
                'display_name' => 'Crear Procedimiento',
                'description' => 'Crear Nuevo Procedimiento'
            ],
            [
                'name' => 'procedimiento-edit',
                'display_name' => 'Editar Procedimiento',
                'description' => 'Editar Procedimiento'
            ],
            [
                'name' => 'procedimiento-delete',
                'display_name' => 'Eliminar Procedimiento',
                'description' => 'Eliminar Procedimiento'
            ],
            [
                'name' => 'reporte-list',
                'display_name' => 'Ver listado de reportes',
                'description' => 'Muestra unicamente el listado de reportes'
            ],
            [
                'name' => 'reporte-export',
                'display_name' => 'Exportar Reporte',
                'description' => 'Exportar Reporte'
            ],
            [
                'name' => 'item-list',
                'display_name' => 'Ver listado de items',
                'description' => 'Muestra unicamente el listado de items'
            ],
            [
                'name' => 'item-create',
                'display_name' => 'Crear Item',
                'description' => 'Crear Nuevo Item'
            ],
            [
                'name' => 'item-edit',
                'display_name' => 'Editar Item',
                'description' => 'Editar Item'
            ],
            [
                'name' => 'item-delete',
                'display_name' => 'Eliminar Item',
                'description' => 'Eliminar Item'
            ],
            [
                'name' => 'entrada-list',
                'display_name' => 'Ver listado de entradas',
                'description' => 'Muestra unicamente el listado de entradas'
            ],
            [
                'name' => 'entrada-create',
                'display_name' => 'Crear Entrada',
                'description' => 'Crear Nueva Entrada'
            ],
            [
                'name' => 'entrada-edit',
                'display_name' => 'Editar Entrada',
                'description' => 'Editar entrada'
            ],
            [
                'name' => 'entrada-delete',
                'display_name' => 'Eliminar Entrada',
                'description' => 'Eliminar entrada'
            ],
            [
                'name' => 'salida-list',
                'display_name' => 'Ver listado de salidas',
                'description' => 'Muestra unicamente el listado de salidas'
            ],
            [
                'name' => 'salida-create',
                'display_name' => 'Crear salida',
                'description' => 'Crear Nuevo salida'
            ],
            [
                'name' => 'salida-edit',
                'display_name' => 'Editar salida',
                'description' => 'Editar salida'
            ],
            [
                'name' => 'salida-delete',
                'display_name' => 'Eliminar salida',
                'description' => 'Eliminar salida'
            ],
            [
                'name' => 'tarea-list',
                'display_name' => 'Ver listado de tareas',
                'description' => 'Muestra unicamente el listado de tareas'
            ],
            [
                'name' => 'tarea-create',
                'display_name' => 'Crear Tarea',
                'description' => 'Crear Nuevo Tarea'
            ],
            [
                'name' => 'tarea-edit',
                'display_name' => 'Editar Tarea',
                'description' => 'Editar Tarea'
            ],
            [
                'name' => 'tarea-delete',
                'display_name' => 'Eliminar Tarea',
                'description' => 'Eliminar Tarea'
            ],
        ];

        $radmin = [
            'name'=>'admin',
            'display_name'=>'Administrador',
            'description'=>'Rol Administrador',
        ];
        //$r = Role::create($radmin);
        $r = Role::find(1);

        foreach ($permission as $key => $value) {
            $p =  Permission::create($value);
            if($r){
                $r->attachPermission($p->id);
            }
        }



    }
}
