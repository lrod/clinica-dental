<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Paciente::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('es_CO');
    return [
        'nombre1' => $faker->firstName,
        'apellido1' => $faker->lastName,
        'apellido2' => $faker->lastName,
        'email' => $faker->safeEmail,
        'identificacion' => $faker->numberBetween(84000000,1124000000),
        'telefono1' => $faker->phoneNumber,
        'direccion' => $faker->address,
        'ocupacion' => $faker->jobTitle,
		'fecha_nacimiento' => $faker->date,               
    ];
});

$factory->define(App\Medico::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('es_CO');
    return [
        'nombre1' => $faker->firstName,
        'apellido1' => $faker->lastName,
        'apellido2' => $faker->lastName,
        'email' => $faker->safeEmail,
        'identificacion' => $faker->numberBetween(84000000,1124000000),
        'telefono1' => $faker->phoneNumber,
        'direccion' => $faker->address,
        'especialidad' => $faker->jobTitle,
    ];
});
