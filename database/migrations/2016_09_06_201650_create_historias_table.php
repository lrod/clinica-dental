<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paciente_id')->unsigned()->index();
            $table->string('estado_salud');
            $table->boolean('opc_atencion');
            $table->boolean('opc_alergias');
            $table->string('alergias');
            $table->boolean('opc_enfermedad');
            $table->string('enfermedad');
            $table->boolean('opc_medicamentos');
            $table->string('medicamentos');
            $table->boolean('opc_dolor');
            $table->string('dolor');
            $table->boolean('opc_extraido');
            $table->string('extraido');
            $table->boolean('opc_ortodoncia');
            $table->timestamps();
            $table->foreign('paciente_id')->references('id')->on('pacientes')->onDelete('cascade');
        });

        Schema::create('examen_facial', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_id')->unsigned()->index();
            $table->string('fac_frente');
            $table->string('fac_perfil');
            $table->string('fac_relacion_labios');
            $table->string('fac_sonrisa');
            $table->string('fac_incisivos');
            $table->string('fac_arco_sonrisa');
            $table->string('fac_contorno_gingival');
            $table->string('fac_linea_media');
            $table->string('fac_linea_sup');
            $table->string('fac_linea_inf');
            $table->timestamps();
            $table->foreign('historia_id')->references('id')->on('historias')->onDelete('cascade');
        });

        Schema::create('examen_dental', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_id')->unsigned()->index();
            $table->string('oclusion_molar_derecha');
            $table->string('oclusion_molar_izquierda');
            $table->string('relacion_canina_derecha');
            $table->string('relacion_canina_izquierda');
            $table->string('sobremordida_vertical');
            $table->string('sobremordida_horizontal');
            $table->string('mordida_cruzada_anterior');
            $table->string('mordida_cruzada_posterior');
            $table->string('alteracion_erupcion_dentaria');
            $table->string('examen_atm');
            $table->string('habitos');
            $table->string('examen_periodontal');
            $table->string('examen_radiografico');
            $table->timestamps();
            $table->foreign('historia_id')->references('id')->on('historias')->onDelete('cascade');
        });

        Schema::create('odontogramas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_id')->unsigned()->index();
            $table->string('inicial');
            $table->string('final');
            $table->timestamps();
            $table->foreign('historia_id')->references('id')->on('historias')->onDelete('cascade');
        });

        Schema::create('ayudas_diagnosticas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_id')->unsigned()->index();
            $table->string('tipo');
            $table->date('fecha');
            $table->string('archivo');
            $table->timestamps();
            $table->foreign('historia_id')->references('id')->on('historias')->onDelete('cascade');
        });

        chema::create('avances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_id')->unsigned()->index();
            $table->date('fecha');
            $table->string('archivo');
            $table->string('descripcion');
            $table->timestamps();
            $table->foreign('historia_id')->references('id')->on('historias')->onDelete('cascade');
        });

        Schema::create('tratamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_id')->unsigned()->index();
            $table->string('diagnostico');
            $table->string('objetivo');
            $table->string('pronostico');
            $table->string('tiempo_estimado');
            $table->timestamps();
            $table->foreign('historia_id')->references('id')->on('historias')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('examen_facial');
        Schema::drop('examen_dental');
        Schema::drop('odontogramas');
        Schema::drop('ayudas_diagnosticas');
        Schema::drop('avances');
        Schema::drop('tratamiento');
        Schema::drop('historias');

    }
}
