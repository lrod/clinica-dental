<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcedimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('servicio_id')->unsigned();
            $table->string('descripcion');
            $table->double('valor');
            $table->foreign('servicio_id')->references('id')->on('servicios')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('paciente_procedimiento', function (Blueprint $table) {
            $table->integer('paciente_id')->unsigned()->index();
            $table->foreign('paciente_id')->references('id')->on('pacientes')->onDelete('cascade');
            $table->integer('procedimiento_id')->unsigned()->index();
            $table->foreign('procedimiento_id')->references('id')->on('procedimientos')->onDelete('cascade');
            $table->double('cuota_inicial');
            $table->timestamp('fecha')->useCurrent=true;
            $table->string('estado');
            $table->date('fecha_cambio')->useCurrent=true;
            $table->string('msg_cambio');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_procedimiento');
        Schema::drop('procedimientos');

    }
}
