<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('medicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_identificacion');
            $table->integer('identificacion')->unsigned()->unique();
            $table->string('apellido1');
            $table->string('apellido2')->nullable();
            $table->string('nombre1');
            $table->string('nombre2')->nullable();
            $table->string('email');
            $table->string('direccion');
            $table->string('telefono1');
            $table->string('telefono2')->nullable();
            $table->string('especialidad');
            $table->boolean('habilitado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('medicos');
    }
}
