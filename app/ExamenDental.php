<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamenDental extends Model
{
    protected $table = 'examen_dental';

    protected $fillable = [];

    protected $guarded =[];

    public function historia(){
        return $this->belongsTo('App\Historia', 'historia_id');
    }
}
