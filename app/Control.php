<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table = 'controles';

    protected $fillable = [];

    protected $guarded =[];

    public function paciente(){
        return $this->belongsTo('App\Paciente');
    }
}
