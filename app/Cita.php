<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = 'citas';

    protected $fillable = ['paciente_id','medico_id','fecha','hora','estado'];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public static $estados =[
        1=>'Atendida',
        2=>'Pendiente',
        3=>'Reprogramada',
        4=>'Cancelada',
    ];
    
     public static $estadosColor =[
        'Atendida' => '#5cb85c',
        'Pendiente' => '#5bc0de',
        'Reprogramada' => '#f0ad4e',
        'Cancelada' => '#d9534f',
    ];

    protected $guarded =[];

    public function paciente(){
        return $this->belongsTo('App\Paciente');
    }

    public function medico(){
        return $this->belongsTo('App\Medico');
    }
}
