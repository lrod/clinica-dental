<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Paciente extends Model
{
    

    protected $table = 'pacientes';

    protected $fillable = ['apellido1', 'apellido2', 'nombre1', 'nombre2', 
    'tipo_identificacion', 'identificacion', 'fecha_nacimiento', 'direccion', 'telefono1', 
    'telefono2','email','ocupacion']; 

    protected $guarded =[];

    public function getRouteKeyName()
    {
        return 'identificacion';
    }

    public function nombreCompleto(){
    	return $this->nombre1.' '.$this->nombre2.' '.$this->apellido1.' '.$this->apellido2;
    }

    public function nombreCorto(){
        return $this->nombre1.' '.$this->apellido1;
    }

    public function edad(){
        $date = $this->fecha_nacimiento;    
    	return Carbon::now()->diff(Carbon::parse($date))->format('%y años, %m meses y %d dias');
    }

    public function historia(){
        return $this->hasOne('App\Historia');
    }

    public function procedimientos(){
        return $this->belongsToMany('App\Procedimiento');
    }

    public function controles(){
        return $this->hasMany('App\Control');
    }

    public function citas(){
        return $this->hasMany('App\Cita');
    }






}
