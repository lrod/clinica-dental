<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historia extends Model
{
    protected $table = 'historias';

    protected $fillable = [];

    protected $guarded =[];

   public function getRouteKeyName()
   {
       return 'id';
   }

    protected $diagnosticas =[
        1=>'Radiografia panoramica',
        2=>'Radiografia de perfil',
        3=>'Modelo de estudios',
        4=>'Fotos',
        5=>'Otros',
    ];

    public function paciente(){
        return $this->belongsTo('App\Paciente', 'paciente_id');
    }

    public function examenFacial(){
        return $this->hasOne('App\ExamenFacial');
    }

    public function examenDental(){
        return $this->hasOne('App\ExamenDental');
    }

    public function odontogramas(){
        return $this->hasOne('App\Odontograma');
    }

    public function diagnosticas(){
        return $this->hasMany('App\AyudaDiagnostica');
    }

    public function avances(){
        return $this->hasMany('App\Avances');
    }

    public function getDiagnosticas()
    {
        return $this->diagnosticas;
    }


}
