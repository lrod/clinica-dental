<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Procedimiento;

class Servicio extends Model
{

    protected $fillable =['nombre'];

    public function getRouteKeyName()
    {
        return 'id';
    }

    protected $guarded = [];

    public function procedimientos(){
        return $this->hasMany('App\Procedimiento');
    }
}
