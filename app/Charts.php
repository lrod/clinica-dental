<?php
/**
 * Created by PhpStorm.
 * User: LuisR
 * Date: 21/09/2016
 * Time: 4:09 PM
 */
use Carbon\Carbon;
use App\Cita;
use App\Control;
use App\Entrada;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;



class Charts
{

    protected  $lava;

    /**
     * ChartsController constructor.
     */
    public function __construct()
    {
        $this->lava = new Lavacharts();
    }

    /**
     * @return Lavacharts
     */
    public function getLava()
    {
        return $this->lava;
    }

    public function getControlesChart($year, $month)
    {

        $days = 30;
        $ccontroles = $this->lava->DataTable();
        $ccontroles->addDateColumn('Dias')
            ->addNumberColumn('Numero de controles');
        for ($day = 1; $day < $days; $day++) {
            $q = Control::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->count();
            $date = Carbon::create($year, $month, $day);
            $ccontroles->addRow(array($date, $total));
        }

        $this->lava->ColumnChart('ControlesMensuales', $ccontroles, [
            'title' => 'Controles Mensuales',
            'legend' => [
                'position' => 'in'
            ]
        ]);
    }

    public function getCitasChart($year, $month)
    {
        $days = 30;
        $ccitas = $this->lava->DataTable();
        $ccitas->addDateColumn('Dias')
            ->addNumberColumn('Numero de citas');
        for ($day = 1; $day < $days; $day++) {
            $q = Cita::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->count();
            $date = Carbon::create($year, $month, $day);
            $ccitas->addRow(array($date, $total));
        }

        $this->lava->ColumnChart('CitasMensuales', $ccitas, [
            'title' => 'Citas Mensuales',
            'legend' => [
                'position' => 'in'
            ]
        ]);
    }

    public function getIngresosChart($year, $month)
    {
        $days = 30;
        $cingresos = $this->lava->DataTable();
        $cingresos->addDateColumn('Dias')
            ->addNumberColumn('Ingreso');
        for ($day = 1; $day < $days; $day++) {
            $q = Control::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->sum('pago');
            $date = Carbon::create($year, $month, $day);
            $cingresos->addRow(array($date, $total));
        }

        $this->lava->ColumnChart('IngresosMensuales', $cingresos, [
            'title' => 'Ingresos del mes',
            'legend' => [
                'position' => 'in'
            ]
        ]);
    }

    public function getGastosChart($year, $month)
    {
        $days = 30;
        $cgastos = $this->lava->DataTable();
        $cgastos->addDateColumn('Dias')
            ->addNumberColumn('Gastos');
        for ($day = 1; $day < $days; $day++) {
            $q = Entrada::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->sum(DB::raw('(precio*cantidad)'));

            $date = Carbon::create($year, $month, $day);
            $cgastos->addRow(array($date, $total));
        }

        $this->lava->ColumnChart('GastosMensuales', $cgastos, [
            'title' => 'Gastos del mes',
            'legend' => [
                'position' => 'in'
            ]
        ]);
    }
}