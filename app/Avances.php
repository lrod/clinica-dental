<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avances extends Model
{
    protected $table = 'avances';

    protected $fillable = ['historia_id','fecha','archivo','descripcion'];

    protected $guarded =[];

    public function historia(){
        return $this->belongsTo('App\Historia', 'historia_id');
    }
}
