<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Servicio;

class Procedimiento extends Model
{

    protected $guarded = [];

    public static $estados =[
        1=>'Activo',
        2=>'Suspendido',
        3=>'Finalizado',
        4=>'Cancelado',
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function servicio(){
        return $this->belongsTo('App\Servicio');
    }

    public static function servicios($id){
        return Procedimiento::where('servicio_id','=', $id)->get();
    }

    public function paciente(){
        return $this->belongsToMany('App\Procedimiento');
    }

}
