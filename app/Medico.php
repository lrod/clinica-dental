<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    
    protected $table = 'medicos';

    public function getRouteKeyName()
    {
        return 'id';
    }

    protected $fillable = ['apellido1', 'apellido2', 'nombre1', 'nombre2', 
    'tipo_identificacion', 'identificacion', 'direccion', 'telefono1', 
    'telefono2','email', 'especialidad', 'habilitado'];

    protected $guarded =[];

    public function nombreCompleto(){
    	return $this->nombre1.' '.$this->nombre2.' '.$this->apellido1.' '.$this->apellido2;
    }

    public function citas(){
        return $this->hasMany('App\Cita');
    }
}
