<?php
/**
 * Created by PhpStorm.
 * User: LuisR
 * Date: 21/09/2016
 * Time: 4:09 PM
 */
use Carbon\Carbon;
use DateTimeZone;
use App\Paciente;
use App\Cita;
use App\Rol;
use App\Control;
use App\Procedimiento;
use App\Item;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;



class Helpers
{


    /**
     * @param $fecha
     * @return static
     */
    public static function getFecha($fecha)
    {
        return Carbon::create($fecha);
    }


    public static function getNombreCompleto($paciente)
    {
        if ($paciente)
            return $paciente->nombre1 . ' ' . $paciente->nombre2 . ' ' . $paciente->apellido1 . ' ' . $paciente->apellido2;
        else
            return '';
    }

    public static function getIdentificacion($paciente)
    {
        if ($paciente)
            return $paciente->identificacion;
        else
            return 0;
    }

    public static function getPacienteByIdentificacion($identificacion)
    {
        return Paciente::where('identificacion', $identificacion)->take(1)->get();
    }

    public static function getPacienteById($id)
    {
        return Paciente::where('id', $id)->get();
    }

    public static function getProcedimientoById($id)
    {
        return Procedimiento::where('id', $id)->get();
    }

    public static function getCitasByDate($fecha)
    {
        return Cita::where('fecha', $fecha)->get();
    }

    public static function getTotalProcedimientos($procedimientos)
    {
        $total = 0;
        foreach ($procedimientos as $proc) {
            $total += $proc->valor;
        }
        return $total;
    }


    public static function getProcedimientosByDate($inicio, $fin)
    {
        $total = 0;
        $procedimientos = DB::table('controles')->where('fecha', $inicio);
        foreach ($procedimientos as $proc) {
            $total += $proc->valor;
        }
        return $total;
    }

    public static function getTotalCuotaInicial($paciente_id)
    {
        $total = 0;
        $cuotas = DB::table('paciente_procedimiento')->select('cuota_inicial')->where('paciente_id', $paciente_id)->get();
        foreach ($cuotas as $cuota) {
            $total += $cuota->cuota_inicial;
        }
        return $total;
    }

    public static function getTotalAbonos($abonos)
    {
        $total = 0;
        foreach ($abonos as $abono) {
            $total += $abono->pago;
        }
        return $total;
    }

    public static function getSaldo($procedimientos, $abonos, $id)
    {
        $total_proc = self::getTotalProcedimientos($procedimientos);
        $total_abon = self::getTotalAbonos($abonos);
        $total_inicial = self::getTotalCuotaInicial($id);
        return $total_proc - $total_abon - $total_inicial;

    }

    public static function getCitaEstados()
    {
        return Cita::$estados;
    }

    public static function getProcedimientoEstados()
    {
        return Procedimiento::$estados;
    }

    public static function getCitaEstadosColor()
    {
        return Cita::$estadosColor;
    }
    
    public static function roles()
    {
        return DB::table('roles')->lists('rol', 'id');
    }

    public static function getMedicosHabilitados()
    {
        $medicos = DB::table('medicos')->where('habilitado', '=', 1)->get();
        return $medicos;
    }

    public static function getTotalItems()
    {
        return DB::table('items')->count();
    }

    public static function getItemsStockMinimo()
    {
        return DB::table('items')->whereRaw('cantidad <= minimo')->get();
    }

    public static function getItemByNombre($nombre)
    {
        return Item::where('producto', $nombre)->get()->first();
    }

    public static function getItemById($id)
    {
        return Item::where('id', $id)->get()->first();
    }

    public static function getMeses()
    {
        return ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    }

    public static function getAnios()
    {
        $year = Carbon::now(new DateTimeZone('America/Bogota'))->year + 1;
        $years = [$year => $year];
        for ($y = $year; $y >= $year - 12; $y--) {
            $years = array_add($years, $y, $y);
        }
        return $years;
    }

}

