<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamenFacial extends Model
{
    protected $table = 'examen_facial';

    protected $fillable = [];

    protected $guarded =[];

    public function historia(){
        return $this->belongsTo('App\Historia', 'historia_id');
    }

}
