<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Odontograma extends Model
{
    protected $table = 'odontogramas';

    protected $fillable = ['historia_id','inicial', 'final'];

    protected $guarded =[];

    public function historia(){
        return $this->belongsTo('App\Historia', 'historia_id');
    }
}
