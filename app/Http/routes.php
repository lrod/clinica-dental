<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('search', 'QueryController@search' );

Route::bind('citas',function($value,$route){
	return App\Cita::whereId($value)->first();
});

Route::bind('servicios',function($value,$route){
	return App\Servicio::whereId($value)->first();
});

Route::bind('procedimientos',function($value,$route){
	return App\Procedimiento::whereId($value)->first();
});

Route::bind('controles',function($value,$route){
	return App\Control::whereId($value)->first();
});



Route::auth();

Route::get('/admin',['middleware' => 'auth', function(){
	return view('admin.dashboard');
}]);

Route::post('/getCitas', 'CitaController@getCitas');

Route::get('/getCitas', 'CitaController@getCitas');

Route::get('/lprocedimientos/{id}', 'ServicioController@getServicios');

Route::get('/getprocedimiento/{id}', 'ProcedimientoController@getProcedimiento');

Route::post('apply/upload', 'ApplyController@upload');

Route::post('apply/uploadDiagnostica', 'ApplyController@uploadDiagnostica');

Route::post('apply/uploadAvance', 'ApplyController@uploadAvance');

Route::post('apply/uploadProfilePicture', 'ApplyController@uploadProfilePicture');

Route::post('addProcedimiento', 'ControlesController@addProcedimiento');

Route::get('apply/getCitas/{year}/{month}/{day}', 'ApplyController@getCitas');

Route::post('addEntrada', 'EntradaController@save');
Route::post('addSalida', 'SalidaController@save');

Route::get('getItem/{nombre}', 'ItemController@getItem');

Route::get('inventario/entradas/search/autocomplete',['uses' => 'QueryController@autocompleteProducto']);

Route::get('inventario/salidas/search/autocomplete',['uses' => 'QueryController@autocompleteProducto']);

Route::get('/reportes/getReporte/{tipo}','ReportesController@getReporte');

Route::get('/reportes/getTotalCitas/{year}/{month}','ReportesController@getCitasStats');

Route::get('/reportes/getReporteMensual/{year}/{month}','ReportesController@getReporteMensual');

Route::get('/reportes/getChartCitasMensuales/{year}/{month}','ReportesController@getChartCitasMensuales');
Route::get('/reportes/getChartControlesMensuales/{year}/{month}','ReportesController@getChartControlesMensuales');
Route::get('/reportes/getChartIngresosMensuales/{year}/{month}','ReportesController@getChartIngresosMensuales');
Route::get('/reportes/getChartGastosMensuales/{year}/{month}','ReportesController@getChartGastosMensuales');

Route::get('/reportes/crearPdf/{tipo}/{accion}/{titulo}', 'PdfController@crearPdf');

Route::get('/lmedicos/habilitados', 'MedicoController@getMedicosHabilitados');

Route::any('/ajax/user/upload-pic/', array('as' => 'uploadImage', 'uses' => 'ApplyController@ajaxUploadImage'));

Route::group(['middleware' => ['auth']], function() {

	Route::get('/home', 'HomeController@index');

	Route::resource('users', 'UserController');
	//Route::resource('pacientes', 'PacienteController');

	Route::get('roles', ['as' => 'roles.index', 'uses' => 'RoleController@index', 'middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
	Route::get('roles/create', ['as' => 'roles.create', 'uses' => 'RoleController@create', 'middleware' => ['permission:role-create']]);
	Route::post('roles/create', ['as' => 'roles.store', 'uses' => 'RoleController@store', 'middleware' => ['permission:role-create']]);
	Route::get('roles/{id}', ['as' => 'roles.show', 'uses' => 'RoleController@show']);
	Route::get('roles/{id}/edit', ['as' => 'roles.edit', 'uses' => 'RoleController@edit', 'middleware' => ['permission:role-edit']]);
	Route::patch('roles/{id}', ['as' => 'roles.update', 'uses' => 'RoleController@update', 'middleware' => ['permission:role-edit']]);
	Route::delete('roles/{id}', ['as' => 'roles.destroy', 'uses' => 'RoleController@destroy', 'middleware' => ['permission:role-delete']]);

	Route::get('medicos', ['as' => 'medicos.index', 'uses' => 'MedicoController@index', 'middleware' => ['permission:medico-list|medico-create|medico-edit|medico-delete']]);
	Route::get('medicos/create', ['as' => 'medicos.create', 'uses' => 'MedicoController@create', 'middleware' => ['permission:medico-create']]);
	Route::post('medicos/create', ['as' => 'medicos.store', 'uses' => 'MedicoController@store', 'middleware' => ['permission:medico-create']]);
	Route::get('medicos/{medico}', ['as' => 'medicos.show', 'uses' => 'MedicoController@show']);
	Route::get('medicos/{medico}/edit', ['as' => 'medicos.edit', 'uses' => 'MedicoController@edit', 'middleware' => ['permission:medico-edit']]);
	Route::patch('medicos/{medico}', ['as' => 'medicos.update', 'uses' => 'MedicoController@update', 'middleware' => ['permission:medico-edit']]);
	Route::delete('medicos/{medico}', ['as' => 'medicos.destroy', 'uses' => 'MedicoController@destroy', 'middleware' => ['permission:medico-delete']]);
	Route::get('medicos/{medico}/habilitar', ['uses'=>'MedicoController@habilitar','middleware' => ['permission:medico-enable']]);

	Route::get('citas', ['as' => 'citas.index', 'uses' => 'CitaController@index', 'middleware' => ['permission:cita-list|cita-create|cita-edit|cita-delete']]);
	Route::get('citas/create', ['as' => 'citas.create', 'uses' => 'CitaController@create', 'middleware' => ['permission:cita-create']]);
	Route::post('citas/create', ['as' => 'citas.store', 'uses' => 'CitaController@store', 'middleware' => ['permission:cita-create']]);
	Route::get('citas/{cita}', ['as' => 'citas.show', 'uses' => 'CitaController@show']);
	Route::get('citas/{cita}/edit', ['as' => 'citas.edit', 'uses' => 'CitaController@edit', 'middleware' => ['permission:cita-edit']]);
	Route::patch('citas/{cita}', ['as' => 'citas.update', 'uses' => 'CitaController@update', 'middleware' => ['permission:cita-edit']]);
	Route::delete('citas/{cita}', ['as' => 'citas.destroy', 'uses' => 'CitaController@destroy', 'middleware' => ['permission:cita-delete']]);
	Route::get('citas/{cita}/cancelar', ['uses'=>'CitaController@cancelar', 'middleware' => ['permission:cita-edit']]);
	Route::get('citas/{cita}/atender', ['uses'=>'CitaController@atender', 'middleware' => ['permission:cita-edit']]);

	Route::get('pacientes', ['as' => 'pacientes.index', 'uses' => 'PacienteController@index', 'middleware' => ['permission:paciente-list']]);
	Route::get('pacientes/create', ['as' => 'pacientes.create', 'uses' => 'PacienteController@create', 'middleware' => ['permission:paciente-create']]);
	Route::post('pacientes/create', ['as' => 'pacientes.store', 'uses' => 'PacienteController@store', 'middleware' => ['permission:paciente-create']]);
	Route::get('pacientes/{paciente}', ['as' => 'pacientes.show', 'uses' => 'PacienteController@show']);
	Route::get('pacientes/{paciente}/edit', ['as' => 'pacientes.edit', 'uses' => 'PacienteController@edit', 'middleware' => ['permission:paciente-edit']]);
	Route::patch('pacientes/{paciente}', ['as' => 'pacientes.update', 'uses' => 'PacienteController@update', 'middleware' => ['permission:paciente-edit']]);
	Route::delete('pacientes/{paciente}', ['as' => 'pacientes.destroy', 'uses' => 'PacienteController@destroy', 'middleware' => ['permission:paciente-delete']]);


	Route::get('pacientes/{paciente}/procedimiento/{procedimiento}', ['as' => 'pacientes.detail', 'uses' => 'PacienteController@detailProcedimiento']);
	Route::get('pacientes/{paciente}/procedimiento/{procedimiento}/terminar', ['uses'=>'ProcedimientoController@terminar', 'middleware' => ['permission:paciente-edit']]);
	Route::get('pacientes/{paciente}/procedimiento/{procedimiento}/suspender', ['uses'=>'ProcedimientoController@suspender', 'middleware' => ['permission:paciente-edit']]);
	Route::get('pacientes/{paciente}/procedimiento/{procedimiento}/reanudar', ['uses'=>'ProcedimientoController@reanudar', 'middleware' => ['permission:paciente-edit']]);
	

	Route::get('servicios', ['as' => 'servicios.index', 'uses' => 'ServicioController@index', 'middleware' => ['permission:servicio-list|servicio-create|servicio-edit|servicio-delete']]);
	Route::get('servicios/create', ['as' => 'servicios.create', 'uses' => 'ServicioController@create', 'middleware' => ['permission:servicio-create']]);
	Route::post('servicios/create', ['as' => 'servicios.store', 'uses' => 'ServicioController@store', 'middleware' => ['permission:servicio-create']]);
	Route::get('servicios/{servicio}', ['as' => 'servicios.show', 'uses' => 'ServicioController@show']);
	Route::get('servicios/{servicio}/edit', ['as' => 'servicios.edit', 'uses' => 'ServicioController@edit', 'middleware' => ['permission:servicio-edit']]);
	Route::patch('servicios/{servicio}', ['as' => 'servicios.update', 'uses' => 'ServicioController@update', 'middleware' => ['permission:servicio-edit']]);
	Route::delete('servicios/{servicio}', ['as' => 'servicios.destroy', 'uses' => 'ServicioController@destroy', 'middleware' => ['permission:servicio-delete']]);

	Route::get('procedimientos', ['as' => 'procedimientos.index', 'uses' => 'ProcedimientoController@index', 'middleware' => ['permission:procedimiento-list|procedimiento-create|procedimiento-edit|procedimiento-delete']]);
	Route::get('procedimientos/create', ['as' => 'procedimientos.create', 'uses' => 'ProcedimientoController@create', 'middleware' => ['permission:procedimiento-create']]);//
	Route::post('procedimientos/create', ['as' => 'procedimientos.store', 'uses' => 'ProcedimientoController@store', 'middleware' => ['permission:procedimiento-create']]);
	Route::get('procedimientos/{id}', ['as' => 'procedimientos.show', 'uses' => 'ProcedimientoController@show']);
	Route::get('procedimientos/{id}/edit', ['as' => 'procedimientos.edit', 'uses' => 'ProcedimientoController@edit', 'middleware' => ['permission:procedimiento-edit']]);
	Route::patch('procedimientos/{id}', ['as' => 'procedimientos.update', 'uses' => 'ProcedimientoController@update', 'middleware' => ['permission:procedimiento-edit']]);
	Route::delete('procedimientos/{id}', ['as' => 'procedimientos.destroy', 'uses' => 'ProcedimientoController@destroy', 'middleware' => ['permission:procedimiento-delete']]);
	


	Route::group(['prefix' => 'inventario'], function() {

		Route::get('items', ['as' => 'items.index', 'uses' => 'ItemController@index', 'middleware' => ['permission:item-list|item-create|item-edit|item-delete']]);
		Route::get('items/create', ['as' => 'items.create', 'uses' => 'ItemController@create', 'middleware' => ['permission:item-create']]);
		Route::post('items/create', ['as' => 'items.store', 'uses' => 'ItemController@store', 'middleware' => ['permission:item-create']]);
		Route::get('items/{item}', ['as' => 'items.show', 'uses' => 'ItemController@show']);
		Route::get('items/{item}/edit', ['as' => 'items.edit', 'uses' => 'ItemController@edit', 'middleware' => ['permission:item-edit']]);
		Route::patch('items/{item}', ['as' => 'items.update', 'uses' => 'ItemController@update', 'middleware' => ['permission:item-edit']]);
		Route::delete('items/{item}', ['as' => 'items.destroy', 'uses' => 'ItemController@destroy', 'middleware' => ['permission:item-delete']]);


		Route::get('entradas', ['as' => 'entradas.index', 'uses' => 'EntradaController@index', 'middleware' => ['permission:entrada-list|entrada-create|entrada-edit|entrada-delete']]);
		Route::get('entradas/create', ['as' => 'entradas.create', 'uses' => 'EntradaController@create', 'middleware' => ['permission:entrada-create']]);
		Route::post('entradas/create', ['as' => 'entradas.store', 'uses' => 'EntradaController@store', 'middleware' => ['permission:entrada-create']]);
		Route::get('entradas/{entrada}', ['as' => 'entradas.show', 'uses' => 'EntradaController@show']);
		Route::get('entradas/{entrada}/edit', ['as' => 'entradas.edit', 'uses' => 'EntradaController@edit', 'middleware' => ['permission:entrada-edit']]);
		Route::patch('entradas/{entrada}', ['as' => 'entradas.update', 'uses' => 'EntradaController@update', 'middleware' => ['permission:entrada-edit']]);
		Route::delete('entradas/{entrada}', ['as' => 'entradas.destroy', 'uses' => 'EntradaController@destroy', 'middleware' => ['permission:entrada-delete']]);

		Route::get('salidas', ['as' => 'salidas.index', 'uses' => 'SalidaController@index', 'middleware' => ['permission:salida-list|salida-create|salida-edit|salida-delete']]);
		Route::get('salidas/create', ['as' => 'salidas.create', 'uses' => 'SalidaController@create', 'middleware' => ['permission:salida-create']]);
		Route::post('salidas/create', ['as' => 'salidas.store', 'uses' => 'SalidaController@store', 'middleware' => ['permission:salida-create']]);
		Route::get('salidas/{salida}', ['as' => 'salidas.show', 'uses' => 'SalidaController@show']);
		Route::get('salidas/{salida}/edit', ['as' => 'salidas.edit', 'uses' => 'SalidaController@edit', 'middleware' => ['permission:salida-edit']]);
		Route::patch('salidas/{salida}', ['as' => 'salidas.update', 'uses' => 'SalidaController@update', 'middleware' => ['permission:salida-edit']]);
		Route::delete('salidas/{salida}', ['as' => 'salidas.destroy', 'uses' => 'SalidaController@destroy', 'middleware' => ['permission:salida-delete']]);

	});


	//Route::get('pacientes/{paciente}/historias', ['as' => 'historias.index', 'uses' => 'HistoriaController@index', 'middleware' => ['permission:paciente-edit']]);
	//Route::get('pacientes/{paciente}/historias/create', ['as' => 'historias.create', 'uses' => 'HistoriaController@create', 'middleware' => ['permission:paciente-edit']]);
	//Route::post('pacientes/{paciente}/historias/create', ['as' => 'historias.store', 'uses' => 'HistoriaController@store', 'middleware' => ['permission:paciente-edit']]);
	//Route::get('pacientes/{paciente}/historias/{historia}', ['as' => 'historias.show', 'uses' => 'HistoriaController@show']);
	//Route::get('pacientes/{paciente}/historias/{historia}/edit', ['as' => 'historias.edit', 'uses' => 'HistoriaController@edit', 'middleware' => ['permission:paciente-edit']]);
	//Route::patch('pacientes/{paciente}/historias/{historia}', ['as' => 'historias.update', 'uses' => 'HistoriaController@update', 'middleware' => ['permission:paciente-edit']]);
	//Route::delete('pacientes/{paciente}/historias/{historia}', ['as' => 'historias.destroy', 'uses' => 'HistoriaController@destroy', 'middleware' => ['permission:paciente-delete']]);


	Route::resource('pacientes.historias', 'HistoriaController');
	Route::resource('pacientes.controles', 'ControlesController');
	Route::resource('pacientes.historias.examenfacial', 'ExamenFacialController');
	Route::resource('pacientes.historias.examendental', 'ExamenDentalController');


	Route::model('pacientes', 'App\Paciente');
//	Route::model('citas', 'Cita');
//	Route::model('historias', 'Historia');
//	Route::model('medicos', 'Medico');
	Route::model('servicios', 'App\Servicio');
	//Route::model('procedimientos', 'App\Procedimiento');
//	Route::model('controles', 'Control');
//	Route::model('examenfacial', 'ExamenFacial');

	Route::resource('servicios', 'ServicioController');
	Route::resource('servicios.procedimientos', 'ProcedimientoController');


	Route::get('reportes', 'ReportesController@index');

	Route::get('reportemensual', 'ReportesController@reporteMensual');

	Route::get('/getChartCitas/{year}/{month}','ChartController@getCitasChart');
	Route::get('/getChartControles/{year}/{month}','ChartController@getControlesChart');
	Route::get('/getChartIngresos/{year}/{month}','ChartController@getIngresosChart');
	Route::get('/getChartGastos/{year}/{month}','ChartController@getGastosChart');


	Route::get('inventario', 'InventarioController@index');

	Route::get('tareas', ['as' => 'tareas.index', 'uses' => 'TareaController@index', 'middleware' => ['permission:tarea-list|tarea-create|tarea-edit|tarea-delete']]);
	Route::get('tareas/create', ['as' => 'tareas.create', 'uses' => 'TareaController@create', 'middleware' => ['permission:tarea-create']]);
	Route::post('tareas/create', ['as' => 'tareas.store', 'uses' => 'TareaController@store', 'middleware' => ['permission:tarea-create']]);
	Route::get('tareas/{tarea}', ['as' => 'tareas.show', 'uses' => 'TareaController@show']);
	Route::get('tareas/{tarea}/edit', ['as' => 'tareas.edit', 'uses' => 'TareaController@edit', 'middleware' => ['permission:tarea-edit']]);
	Route::patch('tareas/{tarea}', ['as' => 'tareas.update', 'uses' => 'TareaController@update', 'middleware' => ['permission:tarea-edit']]);
	Route::delete('tareas/{tarea}', ['as' => 'tareas.destroy', 'uses' => 'TareaController@destroy', 'middleware' => ['permission:tarea-delete']]);

});
