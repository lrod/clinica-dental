<?php


namespace App\Http\Controllers;

//extender tiempo de vida del script
ini_set('max_execution_time', 240);

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;

class PdfController extends Controller
{

    use ReporteTrait;

    public function crearPdf($tipo, $accion, $titulo){

        $view = $this->getViewReport($tipo, $titulo, true)->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHtml($view);

        if($accion==1) return $pdf->stream('reporte');
        if($accion==2) return $pdf->download('reporte.pdf');

    }



}
