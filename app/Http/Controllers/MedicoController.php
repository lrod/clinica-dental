<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Medico;
use Input;
use Redirect;
use Illuminate\Support\Facades\DB;

class MedicoController extends Controller
{


    protected $rules = [       
        'nombre1' => 'required|alpha',
        'nombre2' => 'alpha',
        'apellido1' => 'required|alpha', 
        'apellido2' => 'alpha',
        'identificacion' => 'required|numeric|unique:medicos',
        'telefono1' => 'required|numeric',
        'email' => 'required',
        'direccion' => 'required'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:medico-list|medico-create|medico-edit|medico-delete|medico-enable');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medicos = Medico::all();
        return view('medicos.index', compact('medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medicos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, $this->rules);

        $input = Input::all();
        $input['habilitado']=1;
        Medico::create( $input );
 
        return Redirect::route('medicos.index')->with('message', 'Medico creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Medico $medico)
    {
        return view('medicos.show', compact('medico'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Medico $medico)
    {
        return view('medicos.edit', compact('medico'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Medico $medico, Request $request)
    {
        $this->validate($request, $this->rules);

        $input = array_except(Input::all(), '_method');
        $medico->update($input);

        return Redirect::route('medicos.show', $medico->identificacion)->with('message', 'Medico actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medico $medico)
    {
        $medico->delete();
 
        return Redirect::route('medicos.index')->with('message', 'Medico eliminado.');
    }

    public function habilitar($id)
    {
        $medico = Medico::find($id);
        $habil = $medico->habilitado;
        $medico->habilitado = !$habil;
        $medico->update();
        return Redirect::route('medicos.show', $medico->id)->with('message', 'Medico actualizado.');
    }

    public function getMedicosHabilitados(Request $request){
        if($request->ajax()){
            $medicos = DB::table('medicos')->where('habilitado','=',1)->get();
            return response()->json($medicos);
        }

    }
}
