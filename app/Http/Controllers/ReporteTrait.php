<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

trait ReporteTrait {

    public function getViewReport($tipo, $title, $pdf){
        $isPdf = $pdf;
        switch($tipo){

            case '1':
                $titulo = $title;
                $datos = DB::table('pacientes')
                    ->orderBy('nombre1','asc')
                    ->orderBy('nombre2','asc')
                    ->orderBy('apellido1','asc')
                    ->orderBy('apellido2','asc')
                    ->get();
                return view('reportes.reporte_financiero_pacientes',compact('titulo','datos', 'isPdf'));
            case '2':
                $titulo =  $title;
                $datos = DB::table('pacientes')
                    ->join('controles', 'pacientes.id', '=', 'controles.paciente_id')
                    ->where('fecha', '=', function($query)
                    {
                        $query->select(DB::raw('max(fecha)'))->from('controles')
                            ->whereRaw('paciente_id = pacientes.id');
                    })
                    ->get();
                return view('reportes.reporte_asistencia_pacientes', compact('titulo','datos','isPdf'));


            case '3':
                $titulo =  $title;
                $datos = DB::table('pacientes')
                    ->join('paciente_procedimiento', 'pacientes.id', '=', 'paciente_procedimiento.paciente_id')
                    ->join('procedimientos', 'procedimientos.id', '=', 'paciente_procedimiento.procedimiento_id')
                    ->where('estado', '=', \Helpers::getProcedimientoEstados()[3])->get();
                return view('reportes.reporte_culminados_pacientes', compact('titulo','datos','isPdf'));

            case '4':
                $titulo =  $title;
                $datos = DB::table('paciente_procedimiento')->get();
                return view('reportes.reporte_tratamientos_pacientes', compact('titulo','datos','isPdf'));

        }
    }


}