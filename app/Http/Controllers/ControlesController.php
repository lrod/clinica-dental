<?php

namespace App\Http\Controllers;

use App\Procedimiento;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Paciente;
use Illuminate\Support\Facades\Input;
use App\Control;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ControlesController extends Controller
{

    protected $rules = [
        'fecha' => ['required'],
        'trabajo_realizado' => ['required'],
        'trabajo_pendiente' => ['required'],
        'pago' => ['required'],
    ];

    protected $rules2 = [
        'servicio' => ['required'],
        'procedimiento' => ['required'],
        'cuota_inicial' => ['required'],
        'fecha' => ['required']
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store(Paciente $paciente, Request $request){
        $this->validate($request, $this->rules);
        $input = Input::all();
        //almcenar control - cambiado de paciente_id a paciente_identificacion
        $input['paciente_id'] = $paciente->id;
        Control::create( $input);

        return Redirect::route('pacientes.show', $paciente->identificacion)->with('Control agregado');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addProcedimiento(Request $request){
        $this->validate($request, $this->rules2);
        $input = Input::all();
        //$input['paciente_id'] = $paciente->id;
        //$input['procedimiento_id'] = $procedimiento->id;

        DB::table('paciente_procedimiento')
            ->insert(array(
                    'procedimiento_id'=>$input['procedimiento'],
                    'paciente_id'=>$input['paciente_id'],
                    'cuota_inicial'=>$input['cuota_inicial'],
                    'fecha'=>$input['fecha'],
                    'estado'=> \Helpers::getProcedimientoEstados()[1],  //Estado: Activa
                )
        );
        return Redirect::back()->with('Procedimiento agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
