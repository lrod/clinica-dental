<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Paciente;

use App\Historia;

use App\ExamenFacial;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Input;

class HistoriaController extends Controller
{
    protected $rules = [
        'estado_salud' => ['required', 'min:3'],
    ];

    protected $rules2 = [

    ];

    /**
     * HistoriaController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Paciente $paciente)
    {
        return view('historias.index', compact('paciente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Paciente $paciente)
    {
        return view('historias.create', compact('paciente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Paciente $paciente, Request $request)
    {
        $this->validate($request, $this->rules);
        $input = Input::all();
        $input['paciente_id'] = $paciente->id;
        Historia::create( $input);

        return Redirect::route('pacientes.show', $paciente->identificacion)->with('Historia creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente, Historia $historia)
    {
        return view('historias.show', compact(paciente, historia));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente, Historia $historia)
    {
        return view('historias.edit', compact(paciente, historia));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Paciente $paciente, Request $request, $id)
    {
        $this->validate($request, $this->rules);
        $input = array_except(Input::all(), '_method');
        $historia = $paciente->historia();
        $historia->update($input);
        return Redirect::route('pacientes.show', [$paciente->identificacion, $historia->id])->with('message', 'Historia actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente, Historia $historia)
    {
        $historia->delete();
        return Redirect::route('pacientes.show', $paciente->identificacion)->with('message', 'Historia eliminada.');
    }

    public function createExamenFacial(Paciente $paciente, Request $request)
    {
        $this->validate($request, $this->rules2);
        $input = Input::all();
        $input['historia_id'] = $paciente->historia->id;
        ExamenFacial::create( $input);

        return Redirect::route('pacientes.show', $paciente->identificacion)->with('Historia creada');
    }
}
