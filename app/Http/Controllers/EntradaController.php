<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Entrada;
use Illuminate\Support\Facades\Input;
use App\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class EntradaController extends Controller
{

    protected $rules = [
        'item_id' => 'required',
        'fecha' => 'required|date',
        'cantidad' => 'required|numeric',
        'valor' => 'required|numeric',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entradas = Entrada::all();
        return view('inventario.entradas.index', compact('entradas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventario.entradas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $input = Input::all();
        $input['precio'] = $input['valor'];
        Entrada::create( $input );
        return Redirect::route('entradas.index')->with('message', 'Entrada creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Entrada $entrada)
    {
        return view('inventario.entradas.show', compact('entrada'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrada $entrada)
    {
        return view('inventario.entradas.edit', compact('entrada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Entrada $entrada, Request $request)
    {
        $this->validate($request, $this->rules);
        $input = array_except(Input::all(), '_method');
        $entrada->update($input);
        return Redirect::route('entradas.show', $entrada->id)->with('message', 'Entrada modificada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function save(Request $request)
    {
        $this->validate($request, $this->rules);
        $input = Input::all();

        $item = \Helpers::getItemByNombre($input['producto']);
        if($item){
            $input['item_id'] = $item->id;
            $input['precio'] = $input['valor'];
            $input['fecha'] = new \Carbon\Carbon($input['fecha']);

            $entrada = Entrada::create( $input );
            DB::table('items')->where('id', $item->id)->increment('cantidad', $input['cantidad']);
        }else{
            return Response::json(['producto'=>["No selecciono un producto valido"]],422);
        }
    }
}
