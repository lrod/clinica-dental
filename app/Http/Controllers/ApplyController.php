<?php

namespace App\Http\Controllers;

use App\AyudaDiagnostica;
use App\Avances;
use App\Odontograma;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Helpers;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Paciente;
use Illuminate\Support\Facades\DB;


class ApplyController extends Controller
{
    /**
     * ApplyController constructor.
     * @param array $rules
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function upload(){

        $input = Input::all();
        $ident = Input::get('identificacion');
        $historia = Input::get('historia_id');
        $tipo = Input::get('tipo');
        $file = array('image' => Input::file('image'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);

        if(Input::file('image')->isValid()){
            $destinationPath = 'uploads/odontogramas';
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = 'odontograma_'.$ident.'_'.$tipo.'.'.$extension;
            Input::file('image')->move( $destinationPath, $filename);

            //Guardar la ruta de la archivon del odontograma en DB
            $input[$tipo] = $filename;
            $odontograma = Odontograma::where('historia_id','=',$historia)->first();
            if(!$odontograma){
                Odontograma::create($input);
            }else{
                if($tipo == 'inicial'){
                    $odontograma->inicial = $filename;
                }
                else {
                    $odontograma->final = $filename;
                }
                $odontograma->save();
            }

            Session::flash('success', 'Carga exitosa');
            return Redirect::back()->with(['tab'=>'odontograma']);
            //return Redirect::route('pacientes.index')->with('Odontograma cargado exitosamente');
        }else{
            Session::flash('error', 'Error cargando el odontograma');
            return Redirect::route('pacientes.show', $ident)->with('Odontograma no se pudo cargar');
        }
    }

    public function uploadDiagnostica(){

        $input = Input::all();
        $ident = Input::get('identificacion');
        $tipo = Input::get('tipo');
        $file = array('archivo' => Input::file('archivo'));
        $rules = array('archivo' => 'required');

        $validator = Validator::make($file, $rules);

        if(Input::file('archivo')->isValid()){
            $destinationPath = 'uploads/diagnosticas';
            $extension = Input::file('archivo')->getClientOriginalExtension();
            $filename = $tipo.'_'.$ident.'.'.$extension;
            Input::file('archivo')->move( $destinationPath, $filename);
            $input['archivo'] = $filename;

            AyudaDiagnostica::create($input);

            Session::flash('success', 'Carga exitosa');
            return Redirect::back()->with(['tab'=>'diagnosticas']);
            //return Redirect::route('pacientes.index')->with('Odontograma cargado exitosamente');
        }else{
            Session::flash('error', 'Error cargando el archivo');
            return Redirect::route('pacientes.show', $ident)->with('El archivo no se pudo cargar');
        }
    }

    public function uploadAvance(){

        $input = Input::all();
        $ident = Input::get('identificacion');
        $descripcion = Input::get('descripcion');
        $fecha = Input::get('fecha');
        $file = array('archivo' => Input::file('archivo'));
        $rules = array('archivo' => 'required');
        $tms = Carbon::now()->toTimeString(); 

        $validator = Validator::make($file, $rules);

        if($file && Input::file('archivo')->isValid()){
            $destinationPath = 'uploads/avances';
            $extension = Input::file('archivo')->getClientOriginalExtension();
            $filename = $ident.'_'.$fecha.'_'.$tms.'.'.$extension;
            Input::file('archivo')->move( $destinationPath, $filename);
            $input['archivo'] = $filename;

            Avances::create($input);

            Session::flash('success', 'Carga exitosa');
            return Redirect::back()->with(['tab'=>'diagnosticas']);
            //return Redirect::route('pacientes.index')->with('Odontograma cargado exitosamente');
        }else{
            Session::flash('error', 'Error cargando el archivo');
            return Redirect::route('pacientes.show', $ident)->with('El archivo no se pudo cargar');
        }
    }

    public function uploadProfilePicture(){

        $input = Input::all();
        $ident = Input::get('identificacion');
        $tipo = Input::get('tipo');
        $file = array('archivo' => Input::file('archivo'));
        $rules = array('archivo' => 'required');

        $validator = Validator::make($file, $rules);

        if(Input::file('archivo')->isValid()){
            $destinationPath = 'uploads/profiles';
            $extension = Input::file('archivo')->getClientOriginalExtension();
            $filename = $ident.'.'.$extension;
            Input::file('archivo')->move( $destinationPath, $filename);
            $input['archivo'] = $filename;

            $pac = Paciente::where('identificacion',$ident)->first();
            $pac->foto = $filename;
            $pac->save();

            Session::flash('success', 'Carga exitosa');
            return Redirect::back()->with(['tab'=>'diagnosticas']);
            //return Redirect::route('pacientes.index')->with('Odontograma cargado exitosamente');
        }else{
            Session::flash('error', 'Error cargando el archivo');
            return Redirect::route('pacientes.show', $ident)->with('El archivo no se pudo cargar');
        }
    }

    public function getCitas($year,$month,$day){
        $fecha = Carbon::now();
        $fecha->setDate($year, $month, $day);
        //$from = $fecha->setTime(0,0,0)->toDateTimeString();
        //$to = $fecha->setTime(23,59,59)->toDateTimeString();
        //dd($fecha->toDateString());
        $data = DB::table('citas')->select('hora')
            ->where('fecha','=', $fecha->toDateString())
            ->where('estado','<>','Cancelada')
            ->get();
        $data = json_decode(json_encode($data), true);
        $k=0;
        $sel = [];
        foreach($data as $d){
            foreach($d as $hora){
                $sel[$k] = $hora;
                $k++;
            }
        }
        //print_r($sel);
        $jornada1 =[];
        $jornada2 =[];
        $horaInicio = Carbon::now();
        $horaInicio->setTime(8,0,0);
        for($i=0; $i<8; $i++){
            if(in_array($horaInicio->toTimeString(), $sel, true)) {
                $jornada1[$i] = ['hora' => substr($horaInicio->toTimeString(),0,5),'disponible'=>'false'];
            }else{
                $jornada1[$i] = ['hora' => substr($horaInicio->toTimeString(),0,5),'disponible'=>'true'];
            }
            $horaInicio->addMinutes(30);
        }
        $horaInicio->setTime(14,0,0);
        for($i=0; $i<8; $i++){
            if(in_array($horaInicio->toTimeString(), $sel, true)) {
                $jornada2[$i] = ['hora' => substr($horaInicio->toTimeString(),0,5),'disponible'=>'false'];
            }else{
                $jornada2[$i] = ['hora' => substr($horaInicio->toTimeString(),0,5),'disponible'=>'true'];
            }
            $horaInicio->addMinutes(30);
        }

        return response()->json(array($jornada1,$jornada2));
    }

    public function ajaxUploadImage()
    {
        $file = Input::file('image');
        $input = array('image' => $file);

        $destinationPath = 'uploads/';
        $filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
        Input::file('image')->move($destinationPath, $filename);
        return Response::json(['success' => true, 'file' => asset($destinationPath . $filename)]);

    }


}