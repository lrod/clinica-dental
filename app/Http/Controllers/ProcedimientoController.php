<?php

namespace App\Http\Controllers;

use App\Procedimiento;
use App\Servicio;
use App\Paciente;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTimeZone;
use App\Http\Requests;
use Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ProcedimientoController extends Controller
{
    protected $rules = [
        'nombre' => ['required', 'min:3'],
        'descripcion' => ['required'],
        'valor' => ['required'],
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Servicio $servicio)
    {
        return view('procedimientos.index', compact('servicio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Servicio $servicio)
    {
        return view('procedimientos.create', compact('servicio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Servicio $servicio, Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Input::all();
        $input['servicio_id'] = $servicio->id;
        Procedimiento::create( $input );

        return Redirect::route('servicios.show', $servicio->id)->with('message', 'Servicio creado.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Servicio $servicio, Procedimiento $procedimiento)
    {
        return view('procedimientos.show', compact('servicio', 'procedimiento'));
    }

    

    /**
     *
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Servicio $servicio, Procedimiento $procedimiento)
    {
        return view('procedimientos.edit', compact('servicio', 'procedimiento'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Servicio $servicio, Procedimiento $procedimiento, Request $request)
    {
        $this->validate($request, $this->rules);

        $input = array_except(Input::all(), '_method');
        $procedimiento->update($input);

        return Redirect::route('servicios.procedimientos.show', [$servicio->id, $procedimiento->id])->with('message', 'Procedimiento actualizado.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Servicio $servicio, Procedimiento $procedimiento)
    {
        $procedimiento->delete();

        return Redirect::route('servicios.show', $servicio->id)->with('message', 'Procedimiento eliminado.');

    }


    public function getProcedimiento(Request $request, $id){
        if($request->ajax()){
            $procedimiento = Procedimiento::find($id);
            return response()->json($procedimiento);
        }
    }

    public function terminar(Paciente $paciente, Procedimiento $procedimiento)
    {
        $res = DB::table('paciente_procedimiento')->where('paciente_id',$paciente->id)->update(array('estado' => \Helpers::getProcedimientoEstados()[3], 'fecha_cambio'=> Carbon::now(new DateTimeZone('America/Bogota'))->toDateString()));
       return Redirect::route('pacientes.detail', array($paciente, $procedimiento))->with('message', 'Se ha dado por terminado el tratamiento.');
    }

    public function suspender(Paciente $paciente, Procedimiento $procedimiento)
    {
        DB::table('paciente_procedimiento')->where('paciente_id',$paciente->id)->update(array('estado' => \Helpers::getProcedimientoEstados()[2], 'fecha_cambio'=> Carbon::now(new DateTimeZone('America/Bogota'))->toDateString()));
       return Redirect::route('pacientes.detail', array($paciente, $procedimiento))->with('message', 'Se ha suspendido el tratamiento.');
    }

    public function reanudar(Paciente $paciente, Procedimiento $procedimiento)
    {
        DB::table('paciente_procedimiento')->where('paciente_id',$paciente->id)->update(array('estado' => \Helpers::getProcedimientoEstados()[1],'fecha_cambio'=> Carbon::now(new DateTimeZone('America/Bogota'))->toDateString()));
       return Redirect::route('pacientes.detail', array($paciente, $procedimiento))->with('message', 'Se ha reanudado el tratamiento.');
    }


}
