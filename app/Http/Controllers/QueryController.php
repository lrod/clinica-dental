<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class QueryController extends Controller
{

    protected $selectProd;

    public static function search(Request $request)
    {
        //$query = Input::get('paciente');
        $query = $request->input('buscar');


        //dd($query);

        $pacientes = DB::table('pacientes')->where('identificacion', '=',  $query )->get();

        $medicos = DB::table('medicos')->lists('apellido1', 'id');

        $msg1 = 'No hay paciente registrado con el numero de identificacion: '.$query;

        //return response()->json($pacientes);
        //return ($pacientes);
        //
        //return Redirect::refresh()->with(['pacientes'=>$pacientes]);
        if(count($pacientes)==0){
            $medicos=[];
            return view('citas.create',compact('pacientes','medicos','msg1'));
        }else{            
            $msg1 = '';
            return view('citas.create',compact('pacientes','medicos','msg1'));
        }
    }

    public function autocompleteProducto(){
        $term = Input::get('term');

        $results = array();

        $queries = DB::table('items')
            ->where('producto', 'LIKE', '%'.$term.'%')
            ->take(5)->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->producto ];
        }

        return Response::json($results);
    }

    public function setSelectProduct($select){
        $this->selectProd = $select;
    }

    public function getSelectProduct(){
        return $this->selectProd;
    }
}
