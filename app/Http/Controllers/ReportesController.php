<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;
use App\Cita;
use App\Salida;
use App\Control;
use App\Entrada;
use Charts;
use App\Http\Requests;
use Khill\Lavacharts\Lavacharts;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ReportesController extends Controller
{

    use ReporteTrait;

    protected $reportes;


    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin') ;

        $this->reportes = [
            1=>'Reporte financiero de pacientes',
            2=>'Reporte de asistencia a controles',
            3=>'Reporte pacientes con tratamientos terminados',
            4=>'Reporte de pacientes que iniciaron tratamiento',
        ];
    }


    public function getReporte($tipo){
        return $this->getViewReport($tipo, $this->reportes[$tipo], false);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reportes = $this->reportes;
        return view('reportes.index', compact('reportes'));

    }

    public function reporteMensual()
    {
        $date = Carbon::now(new DateTimeZone('America/Bogota'));
        $year = $date->year;
        $month = $date->month;

        $info = $this->getInfoCitas($year, $month);
        $salidas = $this->getSalidasInventario($year,$month);

        $charts = new Charts();
        $charts->getCitasChart($year,$month);
        $charts->getControlesChart($year,$month);
        $charts->getIngresosChart($year,$month);
        $charts->getGastosChart($year,$month);
        $lava = $charts->getLava();
        return view('reportes.reporte', compact('date','info', 'salidas', 'lava'));
        //return view('reportes.reporte');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    protected function getInfoCitas($year, $month){
        $results = [];
        $total = 0;
        $pendientes = 0;

        $total = Cita::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->count();
        $pendientes = Cita::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->where('estado','Pendiente')->count();
        $reprogramadas = Cita::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->where('estado','Reprogramada')->count();
        $atendidas = Cita::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->where('estado','Atendida')->count();
        $canceladas = Cita::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->where('estado','Cancelada')->count();
        $controles = Control::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->count();
        $totalcontroles = Control::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->sum('pago');


        $tiniciados = DB::table('paciente_procedimiento')
            ->where(DB::raw('MONTH(fecha)'),$month)
            ->where(DB::raw('YEAR(fecha)'),$year)
            ->count();
        //$tterminados = DB::table('paciente_procedimiento')->count();

        $pacientes = $tiniciados + $controles;

        $results = array_add($results,'total', $total);
        $results = array_add($results,'pendientes', $pendientes);
        $results = array_add($results,'reprogramadas', $reprogramadas);
        $results = array_add($results,'atendidas', $atendidas);
        $results = array_add($results,'canceladas', $canceladas);
        $results = array_add($results,'controles', $controles);
        $results = array_add($results,'totalcontroles', $totalcontroles);
        $results = array_add($results,'tiniciados', $tiniciados);
        $results = array_add($results,'pacientes', $pacientes);

        return $results;
    }

    protected function getSalidasInventario($year, $month){
        $items = Item::all();
        $cantidad = 0;
        $salidas = [];

        foreach($items as $item){
            $cantidad = Salida::whereYear('fecha', '=', $year)->whereMonth('fecha', '=', $month)->where('item_id', $item->id)->sum('cantidad');
        if($cantidad)
            $salidas = array_add($salidas, $item->id, ['item'=>$item, 'cantidad'=>$cantidad]);
        }
        return $salidas;
    }

    protected function getCitasByMes($year, $month){
        $q = Cita::whereYear('fecha', '=', $year );
        $citas = $q->WhereMonth('fecha', '=', $month)->get();
        return $citas;
    }

    protected function getCitasStats($year, $month){
        $tcitas = [];
        $days = 30;
        for( $day = 1; $day < $days; $day++){
            $q = Cita::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->count();
            $tcitas = array_add($tcitas, $day, $total);
        }
        return $tcitas;
    }

    protected function getReporteMensual($year, $month){
        $info = $this->getInfoCitas($year, $month);
        $salidas = $this->getSalidasInventario($year, $month);
        return Response::json(['data'=>['info'=>$info, 'salidas'=>$salidas]], 200);
    }

    public static function getChartCitasMensuales($year, $month)
    {
        $lava = new Lavacharts();
        $days = 30;
        $ccitas = $lava->DataTable();
        $ccitas->addDateColumn('Dias')
            ->addNumberColumn('Numero de citas');
        for ($day = 1; $day < $days; $day++) {
            $q = Cita::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->count();
            $date = Carbon::create($year, $month, $day);
            $ccitas->addRow(array($date, $total));
        }

        return $ccitas->toJson();
    }

    public static function getChartControlesMensuales($year, $month)
    {
        $lava = new Lavacharts();
        $days = 30;
        $ccontroles = $lava->DataTable();
        $ccontroles->addDateColumn('Dias')
            ->addNumberColumn('Numero de controles');
        for ($day = 1; $day < $days; $day++) {
            $q = Control::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->count();
            $date = Carbon::create($year, $month, $day);
            $ccontroles->addRow(array($date, $total));
        }

        return $ccontroles->toJson();
    }

    public static function getChartIngresosMensuales($year, $month)
    {
        $lava = new Lavacharts();
        $days = 30;
        $cingresos = $lava->DataTable();
        $cingresos->addDateColumn('Dias')
            ->addNumberColumn('Ingresos');
        for ($day = 1; $day < $days; $day++) {
            $q = Control::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->sum('pago');
            $date = Carbon::create($year, $month, $day);
            $cingresos->addRow(array($date, $total));
        }

        return $cingresos->toJson();
    }

    public static function getChartGastosMensuales($year, $month)
    {
        $lava = new Lavacharts();
        $days = 30;
        $cgastos = $lava->DataTable();
        $cgastos->addDateColumn('Dias')
            ->addNumberColumn('Ingresos');
        for ($day = 1; $day < $days; $day++) {
            $q = Entrada::whereYear('fecha', '=', $year);
            $q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->sum(DB::raw('precio*cantidad'));
            $date = Carbon::create($year, $month, $day);
            $cgastos->addRow(array($date, $total));
        }

        return $cgastos->toJson();
    }

    public static function getChartGastosAnuales($year)
    {
        $lava = new Lavacharts();
        $days = 30;
        $cgastosy = $lava->DataTable();
        $cgastosy->addDateColumn('Mes')
            ->addNumberColumn('Ingresos');
        for ($day = 1; $day < $days; $day++) {
            $q = Entrada::whereYear('fecha', '=', $year);
            //$q->whereMonth('fecha', '=', $month);
            $q->whereDay('fecha', '=', $day);
            $total = $q->sum(DB::raw('precio*cantidad'));
            $date = Carbon::create($year, $month, $day);
            $cgastosy->addRow(array($date, $total));
        }

        return $cgastosy->toJson();
    }

}
