<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salida;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class SalidaController extends Controller
{

    protected $rules = [
        'producto' => 'required',
        'fecha' => 'required|date',
        'cantidad' => 'required|numeric',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salidas = Salida::all();
        return view('inventario.salidas.index', compact('salidas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventario.salidas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $input = Input::all();
        Salida::create( $input );
        return Redirect::route('inventario.salidas.index')->with('message', 'Salida de inventario agregada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Salida $salida)
    {
        return view('inventario.salidas.show', compact('salida'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Salida $salida)
    {
        return view('inventario.salidas.edit', compact('salida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Salida $salida, Request $request)
    {
        $this->validate($request, $this->rules);
        $input = array_except(Input::all(), '_method');
        $salida->update($input);
        return Redirect::route('inventario.salidas.show', $salida->id)->with('message', 'Salida de inventario modificada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function save(Request $request)
    {
        $this->validate($request, $this->rules);
        $input = Input::all();
        $item = \Helpers::getItemByNombre($input['producto']);
        if($item){
            $input['item_id'] = $item->id;
            $input['fecha'] = new \Carbon\Carbon($input['fecha']);

            if($input['cantidad']> $item->cantidad){
                return Response::json(['cantidad' =>["La cantidad ingresada es mayor que las existencias"]],422);
            }

            $salida = Salida::create( $input );
            DB::table('items')->where('id', $item->id)->decrement('cantidad', $input['cantidad']);
            return Response::json($salida, 200);
        }else{
            return Response::json(['producto'=>["No selecciono un producto valido"]],422);
        }

    }

}
