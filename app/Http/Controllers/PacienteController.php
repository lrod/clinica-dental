<?php

namespace App\Http\Controllers;

use App\Servicio;
use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Http\Requests;

use App\Paciente;

use App\Procedimiento;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Redirect;


class PacienteController extends Controller
{




    protected $rules = [       
        'nombre1' => 'required|alpha',
        'nombre2' => 'alpha',
        'apellido1' => 'required|alpha', 
        'apellido2' => 'alpha',
        'identificacion' => 'required|numeric|unique:pacientes',
        'fecha_nacimiento' => 'required|date',
        'telefono1' => 'required|numeric',
        'email' => 'required',
        'direccion' => 'required', 
        'sexo' => 'required'
    ];

    /**
     * PacienteController constructor.
     */
    public function __construct()
    {
        //$this->middleware('auth');
        //$this->middleware(['permission:paciente-list','permission:paciente-create','permission:paciente-edit','permission:paciente-delete']);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pacientes = Paciente::where(function($query) use($request){
            if(($buscar = $request->get('buscar'))){
                if($request->get('criterio')=='name') {
                    $query->orWhere('nombre1', 'LIKE', '%' . $buscar . '%')
                        ->orWhere(DB::raw("CONCAT(`nombre1`,' ',`nombre2`)"), 'LIKE', "%" . $buscar . "%")
                        ->orWhere(DB::raw("CONCAT(`nombre1`,' ',`apellido1`)"), 'LIKE', "%" . $buscar . "%")
                        ->orWhere(DB::raw("CONCAT(`apellido1`,' ',`apellido1`)"), 'LIKE', "%" . $buscar . "%")
                        ->orWhere(DB::raw("CONCAT(`nombre1`,' ',`apellido1`,' ',`apellido2`)"), 'LIKE', "%" . $buscar . "%")
                        ->orWhere(DB::raw("CONCAT(`nombre1`,' ',`nombre2`,' ',`apellido1`,' ',`apellido2`)"), 'LIKE', "%" . $buscar . "%");
                }else{
                    $query->where('identificacion', $buscar);
                }
            }
        })->orderBy('nombre1','asc')
            ->orderBy('nombre2','asc')
            ->orderBy('apellido1','asc')
            ->orderBy('apellido2','asc')
        ->paginate(10);
        return view('pacientes.index', compact('pacientes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pacientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Input::all();
        $pac = Paciente::create( $input );
        $servicios = Servicio::lists('nombre', 'id');
        return Redirect::route('pacientes.show',['paciente'=>$pac,'servicios'=>$servicios])->with('message', 'Paciente created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
        $servicios = Servicio::lists('nombre', 'id');
        //$procedimientos_practicados = Paciente::
        return view('pacientes.show', compact('paciente','servicios'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailProcedimiento(Paciente $paciente, Procedimiento $procedimiento)
    {
        Carbon::setLocale('es');
        $datos = DB::table('paciente_procedimiento')->where('paciente_id', $paciente->id)->get();
        $fecha = DB::table('controles')->where('paciente_id', $paciente->id)->max('fecha');

        $datos['duracion']=Carbon::parse($datos[0]->fecha)->diffForHumans();
        $datos[0]->fecha = Carbon::parse($datos[0]->fecha)->toDateString();

        $datos['ultimo_control']=$fecha;
        $datos['inactividad']=Carbon::parse($fecha)->diffForHumans();
        
        return view('pacientes.detail', compact('paciente', 'procedimiento', 'datos'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        return view('pacientes.edit', compact('paciente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Paciente $paciente, Request $request)
    {
        $this->validate($request, $this->rules);

        $input = array_except(Input::all(), '_method');
        $paciente->update($input);
 
        return Redirect::route('pacientes.show', $paciente->identificacion)->with('message', 'Paciente updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        $paciente->delete();
 
        return Redirect::route('pacientes.index')->with('message', 'Paciente deleted.');

    }


    /**
     * Search for a paciente.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        echo 'buscando';

        $query = Request::input('search');

        $pacientes = DB::table('pacientes')->where('nombre1', 'LIKE', '%' . $query . '%')->paginate(10);

        return dd($pacientes);

    }

   
}
