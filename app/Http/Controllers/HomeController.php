<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Helpers;
use DateTimeZone;
use App\Tarea;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citas = Helpers::getCitasByDate(\Carbon\Carbon::now(new DateTimeZone('America/Bogota'))->toDateString());
        $tareas = Tarea::all();
        return view('home', compact('citas', 'tareas'));
    }

    public function admin()
    {
        return view('admin');
    }
}
