<?php

namespace App\Http\Controllers;

use App\Paciente;
use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon\Carbon;

use App\Cita;

use Input;

use Response;

use Redirect;

class CitaController extends Controller
{

    protected $rules = [
        'paciente_id' => 'required',
        'paciente' => 'required',
        'medico_id' => 'required',
        'fecha' => 'required|date',
        'hora' => 'required',
    ];

    /**
     * CitaController constructor.
     * @param array $rules
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $citas = Cita::where(function($query) use($request){
           // dd($request);
           //dd($request->get('filtro-cita'));
            if(('medico' == $request->get('filtro-cita'))){
                $filtro = $request->get('filtro');
                $query->orWhere('medico_id','=', $filtro);
            }else if(('estado' == $request->get('filtro-cita'))){
                $filtro = $request->get('filtro');
                $query->orWhere('estado','=', $filtro);
            }
        })->orderBy('fecha','desc')
            ->paginate(5);
        return view('citas.index', compact('citas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pacientes = [];
        $medicos = [];
        $msg1 ='';
        return view('citas.create', compact('pacientes','medicos','msg1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Input::all();
        //$input['fecha'] = Input::get('fecha_cita');

        Cita::create( $input );

        return Redirect::route('citas.index')->with('message', 'Cita agendada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cita $cita)
    {
        return view('citas.show')->with(['cita'=>$cita]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cita $cita)
    {
        return view('citas.edit')->with(['cita'=>$cita]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Cita $cita, Request $request)
    {
        $this->validate($request, $this->rules);
        $input = array_except(Input::all(), '_method');
        $cita->update($input);
        return Redirect::route('citas.index', [$cita->id])->with('message', 'Cita reprogramada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancelar($id)
    {
        $cita = Cita::find($id);
        $cita->estado = \Helpers::getCitaEstados()[4];  //estado: Cancelada
        $cita->update();
        return Redirect::route('citas.index', $cita->id)->with('message', 'La cita '.$cita->id.' ha sido cancelada.');
    }

    public function atender($id)
    {
        $cita = Cita::find($id);
        $cita->estado = \Helpers::getCitaEstados()[1];  //estado: Atendida
        $cita->update();
        return Redirect::route('citas.index', $cita->id)->with('message', 'La cita '.$cita->id.' ha sido atendida.');
    }

    public function getCitas(){
        $citas = Cita::all();
        $tiempo_atencion = 30;
        $datos = [];
        $colores = \Helpers::getCitaEstadosColor();
        foreach ($citas as $cita) {
            $date = Carbon::parse($cita->fecha.' T'.$cita->hora);            
            $datos[] = 
                ['start' => $date->toDateTimeString(),
                'end' => $date->addMinutes($tiempo_atencion)->toDateTimeString(),
                'title' => $cita->paciente->nombreCorto(),
                'url' => route('citas.show', $cita),
                'color' => $colores[$cita->estado]
            ];
        }
        return Response::json($datos);
    }

}
