<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AyudaDiagnostica extends Model
{
    protected $table = 'ayudas_diagnosticas';

    protected $fillable = ['historia_id','tipo', 'fecha', 'archivo'];

    protected $guarded =[];

    public function historia(){
        return $this->belongsTo('App\Historia', 'historia_id');
    }
}
