/**
 * Created by LuisR on 05/09/2016.
 */

$("#servicio").change(function(event){
    console.log(event.target.value);
    $.get("/lprocedimientos/"+event.target.value+"", function(response){
        console.log(response);
        $("#procedimiento").empty();
        $("#procedimiento").append("<option value='0' disabled selected>Seleccione" + "</option>");
        for (i = 0; i < response.length; i++) {
            $("#procedimiento").append("<option value='" + response[i].id + "'> " + response[i].nombre + "</option>");
        }
    })
    console.log("val:"+$("#procedimiento option:selected").val());
    if( $("#procedimiento option:selected").val()>-1){
        selectProcedimiento();
    }
});

$("#procedimiento").change(function(event){
    selectProcedimiento();
});

function selectProcedimiento()
{

    var procId = $("#procedimiento option:selected").val();
    $.ajax({
        type: 'GET',
        url: '/getprocedimiento/' + procId,
        data: {get_param: 'value'},
        success: function (data) {
            console.log(data.valor);
            $('input[name="valor_proc"]').val(""+data.valor);
            console.log($("#valor_proc"));
        }
    });
}

$("#datepicker").on("change", function () {
    console.log("Fecha:"+$(this).val());

    var myDate = new Date($(this).val()+" 00:00:00 GMT-0500");
    console.log(myDate, myDate.getTime());
    $day = myDate.getDate();
    $month = myDate.getMonth()+1;
    $year = myDate.getFullYear();
    var date = moment(myDate);
    $fecha = moment(myDate).format("dddd, Do MMMM YYYY");
    console.log($fecha);
    $("#tiempo").text($fecha);
    console.log("/apply/getCitas/"+$year+"/"+$month+"/"+$day+"");
    $.get("/apply/getCitas/"+$year+"/"+$month+"/"+$day+"", function(response){
        //console.log(response);
        $("#jornada1").empty();
        for (i = 0; i < response[0].length; i++) {
            $val = "<label class='radio-inline"+(response[0][i].disponible=='true'?"'>":" disabled'>") +
                "<input name='hora' type='radio' value='" + response[0][i].hora + "' id='hora'" + (response[0][i].disponible=='true'?" >":" disabled=disabled >") + response[0][i].hora
                + "</label>";
            $("#jornada1").append($val);
        }
        $("#jornada2").empty();
        for (i = 0; i < response[1].length; i++) {
            $val = "<label class='radio-inline"+(response[1][i].disponible=='true'?"'>":" disabled'>") +
                "<input name='hora' type='radio' value='" + response[1][i].hora + "' id='hora'" + (response[1][i].disponible=='true'?" >":" disabled=disabled >") + response[1][i].hora
                + "</label>";
            $("#jornada2").append($val);
        }

    })
});


$(function(){
    var your_dates = [new Date(2016, 9, 7),new Date(2016, 10, 8)];
    $('#calendario').datepicker({
        beforeShowDay: function(date) {
            // check if date is in your array of dates
            if($.inArray(date, your_dates)) {
                // if it is return the following.
                return [true, 'css-class-to-highlight', 'tooltip text'];
            } else {
                // default
                return [true, '', ''];
            }
        }
    });
});



/*var calendar = $("#calendario").calendar(
    {
        tmpl_path: "/tmpls/",
        events_source: function () { return []; }
    });
    */

function cargarReporte(reporte){

    //funcion para cargar los diferentes  en general
    var url = "/reportes/getReporte/"+reporte;


    //$("#contenido_reporte").html($("#cargador_empresa").html());
    $.get(url,function(result){
        $("#contenido_reporte").html(result);
    })

}

$("#filtro-cita").change(function(event){

        var val = event.target.value;

        console.log(val);

        if(val=="medico"){
            $.get("lmedicos/habilitados", function(response){
                console.log(response.length);
                $("#filtro").empty();
                $("#filtro").append("<option disabled selected>Seleccione" + "</option>");
                for (i = 0; i < response.length; i++) {
                    $("#filtro").append("<option value='" + response[i].id + "'> " + response[i].apellido1 + "</option>");
                }
            })
        }else if(val=="estado") {
            $("#filtro").empty();
            $("#filtro").append("<option disabled selected>Seleccione" + "</option>");
            $("#filtro").append("<option>Pendiente" + "</option>");
            $("#filtro").append("<option>Atendida" + "</option>");
            $("#filtro").append("<option>Cancelada" + "</option>");
        }

});

$( "#in_cantidad" ).on('input', function() {
    calcularTotal(false);
}).focusout(function() {
    calcularTotal(false);
}).keyup(function(e){
    calcularTotal(false);
});

$( "#in_valor" ).on('input', function() {
    calcularTotal(false);
}).focusout(function() {
    calcularTotal(false);
}).keyup(function(e){
    calcularTotal(false);
});

/*$(function() {
    $( "#in_fecha" ).datepicker({ dateFormat: 'dd-mm-yy'});
    $( "#out_fecha" ).datepicker({ dateFormat: 'dd-mm-yy'});
});
*/

$(function()
{
    $( "#in_producto" ).autocomplete({
        source: "search/autocomplete",
        minLength: 3,
        select: function(event, ui) {
            $('#in_producto').val(ui.item.value);
        }
    });

    $( "#out_producto" ).autocomplete({
        source: "search/autocomplete",
        minLength: 3,
        select: function(event, ui) {
            $('#out_producto').val(ui.item.value);
            var prd = $('#out_producto').val();
            $.get("/getItem/"+prd+"", function(response) {
                $('#info-producto').empty();
                $('#info-producto').append(
                    '<div class="row">'+
                    '<div class="col-sm-6">'+response.producto+'</div>'+
                    '<div class="col-sm-4 pull-right">'+response.cantidad+' Unidades disponibles</div>'+
                    '</div>'
                );
                console.log(response);
            });
        }
    });
});

$( "#out_cantidad" ).on('input', function() {
    calcularTotal(true);
}).focusout(function() {
    calcularTotal(true);
}).keyup(function(e){
    calcularTotal(true);
});

$( "#out_valor" ).on('input', function() {
    calcularTotal(true);
}).focusout(function() {
    calcularTotal(true);
}).keyup(function(e){
    calcularTotal(true);
});

function calcularTotal(out){
    var cant =0;
    var valor =0;
    var total = 0;
    if(out){
        cant = parseFloat($("#out_cantidad").val());
        valor = parseFloat($("#out_valor").val());
    }else{
        cant = parseFloat($("#in_cantidad").val());
        valor = parseFloat($("#in_valor").val());
    }
    if(cant>=0 && valor>=0){
        total = cant * valor;
    }
    if(out)
        $("#out_total").val(total);
    else
        $("#in_total").val(total);

}

function getProductoIn(){
    var prod = $('#in_producto').val();
    return prod;
}

function getProductoOut(){
    var prod = $('#out_producto').val();
    return prod;
}

$( '#form_entrada').on( 'submit', function(e) {

    $.ajaxSetup({
        header:$('meta[name="_token"]').attr('content')
    })
    e.preventDefault(e);

    var url = '/addEntrada';
    var prod = getProductoIn();
    var cantidad = $('#in_cantidad').val();
    var precio = $('#in_valor').val();
    var fecha = $('#in_fecha').val();
    var data = $(this).serializeArray(); // convert form to array
    data.push({name: "producto", value: prod});

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(data){
            window.location.href = "/inventario/entradas";
            //console.log(data);
        },
        error: function(data){
            if( data.status === 422 ) {
                $errors = data.responseJSON;
                errorsHtml = '<div class="alert alert-danger"><ul>';
                $.each($errors, function (key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul></di>';
                $('#form-errors').html(errorsHtml);
            }
        }
    });
});

$( '#form_salida').on( 'submit', function(e) {

    $.ajaxSetup({
        header:$('meta[name="_token"]').attr('content')
    })
    e.preventDefault(e);

    var url = '/addSalida';
    var prod = getProductoOut();
    var cantidad = $('#out_cantidad').val();
    var precio = $('#out_valor').val();
    var fecha = $('#out_fecha').val();
    var data = $(this).serializeArray(); // convert form to array
    data.push({name: "producto", value: prod});

     $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(data){
            window.location.href = "/inventario/salidas";
            //console.log(data);
        },
        error: function(data){
            //console.log(data);
            if( data.status === 422 ) {
                $errors = data.responseJSON;
                errorsHtml = '<div class="alert alert-danger"><ul>';
                $.each($errors, function (key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul></di>';
                $('#form-errors').html(errorsHtml);
            }
            //alert(data['responseText']);
        }
    });
});


$( '#form_reporte').on( 'submit', function(e) {
    e.preventDefault();
    consultaReporteMensual();
});

$(function() {
    consultaReporteMensual();
});

/*$("#select_mes").datepicker( {
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
});
*/

$("#year-stats").change(function(event){
    consultaReporteMensual();
});

$("#month-stats").change(function(event) {
    consultaReporteMensual();
});

function consultaReporteMensual(){
    var year = $("#year-stats option:selected").val();
    var month = $("#month-stats option:selected").val();
    if(year>-1 && month >-1){
        reporteMensual(year, parseInt(month)+1);
    }
}

function reporteMensual(year, month){
    console.log('target:'+year+'-'+month);
    $.get("/reportes/getReporteMensual/"+year+"/"+month+"", function(response){
        data = response['data'];
        info = data['info'];
        inv = data['salidas'];
        $('#info-citas').empty();
        $('#info-citas').append(
            "<div>Total:         "+info['total']+" </div>"+
            "<div>Atendidas:     "+info['atendidas']+" </div>"+
            "<div>Pendientes:    "+info['pendientes'] +" </div>"+
            "<div>Reprogramadas: "+info['reprogramadas'] +" </div>"+
            "<div>Canceladas:    "+info['canceladas']+" </div>"
        );

        $('#info-controles').empty();
        $('#info-controles').append(
            "<div>Total:  "+info['controles']+"</div>"
        );

        $('#info-tratamientos').empty();
        $('#info-tratamientos').append(
            "<div>Total:  "+info['tiniciados']+"</div>"
        );

        $('#info-pacientes').empty();
        $('#info-pacientes').append(
            "<div>Total:  "+info['pacientes']+"</div>"
        );

        $('#info-inventario-out').empty();
        $.each( inv, function( key, value ) {
            $('#info-inventario-out').append("" +
                "<tr><td>"+value['item']['codigo']+"</td>"+
                "<td>"+value['item']['producto']+"</td>"+
                "<td>"+value['cantidad']+"</td>"+
                "</tr>"
            );
        });

        $.getJSON("/reportes/getChartCitasMensuales/"+year+"/"+month+"", function (dataTableJson) {
            lava.loadData('CitasMensuales', dataTableJson, function (chart) {
                //console.log(chart);
            });
        });

        $.getJSON("/reportes/getChartControlesMensuales/"+year+"/"+month+"", function (dataTableJson) {
            lava.loadData('ControlesMensuales', dataTableJson, function (chart) {
                //console.log(chart);
            });
        });

        $.getJSON("/reportes/getChartIngresosMensuales/"+year+"/"+month+"", function (dataTableJson) {
            lava.loadData('IngresosMensuales', dataTableJson, function (chart) {
                //console.log(chart);
            });
        });

        $.getJSON("/reportes/getChartGastosMensuales/"+year+"/"+month+"", function (dataTableJson) {
            lava.loadData('GastosMensuales', dataTableJson, function (chart) {
                //console.log(chart);
            });
        });

    })
}

$('#fullcalendario').fullCalendar({

    locale: 'es',
    events: {
        url: '/getCitas',
        type: 'GET',
        data: {
            custom_param1: 'something',
            custom_param2: 'somethingelse'
        },
        error: function() {
            alert('there was an error while fetching events!');
        },
        color: '#5cb85c',   // a non-ajax option
        textColor: 'black' // a non-ajax option
    }

});