@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Editar entrada</div>

        <div class="panel-body">

            {!! Form::model($entrada, ['method' => 'PATCH', 'route' => ['entradas.update', $entrada->id]]) !!}
            @include('inventario/entradas/partials/_form', ['submit_text' => 'Editar entrada'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection
