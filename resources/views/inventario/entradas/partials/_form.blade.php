{!! Form::hidden('item_id', empty($item)?0:$item->id)  !!}
<div class="form-group col-sm-3 pull-right">
    {!! Form::label('fecha', 'Fecha:', ['class' => 'control-label']) !!}
    {!! Form::date('fecha', null, ['class' => 'form-control', 'id'=>'in_fecha']) !!}
</div>
<!--div class="form-group col-sm-6">
    {-- Form::label('Proveedor', 'Proveedor:', ['class' => 'control-label']) !!}
    {-- Form::text('proveedor', null, ['class' => 'form-control']) !!}
</div >
<hr>
<div class="form-group col-sm-12">
    {-- Form::label('producto', 'Producto:', ['class' => 'control-label']) !!}
    <div class="input-group">

    {-- Form::text('producto', null, ['class' => 'form-control', 'id'=>'in_producto']) !!}
        <div class="input-group-btn">
        {-- Form::submit('Buscar',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
</div -->

<div class="form-group col-sm-4">
    {!! Form::label('cantidad', 'Cantidad:', ['class' => 'control-label']) !!}
    {!! Form::number('cantidad', null, ['class' => 'form-control', 'id' => 'in_cantidad']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('valor', 'Valor unitario:', ['class' => 'control-label']) !!}
    {!! Form::number('valor', null, ['class' => 'form-control','id' => 'in_valor']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('total', 'Valor total:', ['class' => 'control-label']) !!}
    {!! Form::text('total', null, ['class' => 'form-control','id' => 'in_total', 'readonly'] ) !!}
</div>
<div class="form-group col-sm-6">
    <div class="pull-right">
        {!! Form::submit($submit_text, ['class'=>'btn btn-primary']) !!}
    </div>
</div>