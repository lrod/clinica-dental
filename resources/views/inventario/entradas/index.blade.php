@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Entradas de inventario</div>

        <div class="panel-body">
            <div class="pull-right">
                <div class="btn-group">
                    <a class="btn btn-primary" href="{{ url('inventario') }}"> Inventario</a>
                    @permission('entrada-create')
                    <a class="btn btn-success" href="{{route('entradas.create') }}">Crear Entrada</a>
                    @endpermission
                </div>
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                <th>Codigo</th>
                <th>Fecha</th>
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Valor</th>
                <th>Total</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($entradas as $entrada)
                    @php($prod = \Helpers::getItemById($entrada->item_id))
                    @php($fecha = new \Carbon\Carbon($entrada->fecha))
                    <tr>
                        <td>{{$entrada->id}}</td>
                        <td>{{$fecha->formatLocalized('%d-%m-%Y')}}</td>
                        <td><a href="{{ route('items.show', $entrada->item_id) }}">{{ $prod!==null?$prod->producto:'XXX' }}</a></td>
                        <td class="valor">{{$entrada->cantidad}}</td>
                        <td class="valor">{{$entrada->precio}}</td>
                        <td class="valor">{{$entrada->cantidad*$entrada->precio}}</td>
                        @permission('entradas-edit')
                        <td> {!! link_to_route('entradas.edit', 'Editar', array($entrada->id), array('class' => 'btn btn-default')) !!} </td>
                        @endpermission
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop