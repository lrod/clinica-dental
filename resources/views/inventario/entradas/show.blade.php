@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading ">
            <span class="fa fa-book fa-fw"></span>
            Informacion de la entrada
            <div class="pull-right">
                <span class="glyphicon glyphicon-arrow-left"></span>
                {!! link_to_route('entradas.index', 'Regresar al listado de entradas') !!}
            </div>
        </div>

        <div class="panel-body">
            @if($item !== null)
                <div class="">
                    <i></i>
                </div>
                <div class="pull-left">
                    <h4>{{ $entrada->codigo }}</h4>
                    <h3>{{ $etrada->fecha }}</h3>
                </div>
                <div class=" pull-right">
                    <div class="btn-group-vertical">
                        @permission('entrada-edit')
                        <a class="btn btn-info" href="{{route('items.edit', $entrada->id)}}">Editar</a>
                        @endpermission
                        @permission('entrada-delete')
                        {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('items.destroy', $entrada->id))) !!}
                        {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                        {!! Form::close() !!}
                        @endpermission
                    </div>
                </div>
        </div>
        <table class="table table-striped" width="100%">


        </table>
    </div>


    @endif

@stop