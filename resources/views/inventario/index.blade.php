@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Inventario</div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                            <div class="panel-title col-sm-6">
                                <h3><span class="fa fa-book fa-fw"></span><a href="{{route('items.index')}}">Items</a></h3>
                            </div>
                            <div class="huge col-sm-6 text-right">{{Helpers::getTotalItems()}}</div>
                                @permission('item-create')
                                <a class="btn btn-success" href="{{ route('items.create') }}"> Crear Nuevo Item</a>
                                @endpermission
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                            <div class="panel-title col-sm-12">
                                <h3><span class="fa fa-sign-in fa-fw"></span><a href="{{route('entradas.index')}}">Entradas</a></h3>
                            </div>
                            <!--div class="huge col-sm-6 text-right"></div-->
                            @permission('entrada-create')
                            <a class="btn btn-success" href="{{ route('entradas.create') }}"> Agregar entrada</a>
                            @endpermission
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                            <div class="panel-title col-sm-12">
                                <h3><span class="fa fa-sign-out fa-fw"></span><a href="{{route('salidas.index')}}">Salidas</a></h3>
                            </div>
                            <!--div class="huge text-right col-sm-6"></div-->
                            @permission('salida-create')
                            <a class="btn btn-success" href="{{ route('salidas.create') }}"> Agregar salida</a>
                            @endpermission
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="col-sm-12 pull-right">
                        <div class="panel panel-info">
                            <div class="panel-heading">Productos con stock minimo</div>
                            <div class="panel-body">
                            @php($items = Helpers::getItemsStockMinimo())
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Producto</th>
                                        <th>Minimo</th>
                                        <th>Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($items as $item)
                                        <tr>
                                            <th>{{strtoupper($item->codigo)}}</th>
                                            <th><a href="{{ route('items.show', $item->id) }}">{{$item->producto}}</a></th>
                                            <th class="text-right">{{$item->minimo}}</th>
                                            <th class="text-right">{{$item->cantidad}}</th>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </div>
    </div>

@stop