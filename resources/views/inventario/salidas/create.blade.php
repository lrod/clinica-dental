@extends('layouts.app')

@section('content')

    <div id="form-errors">

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Registrar Salida</div>
        <div class="panel-body">

            @if(Request::has('producto'))
                @php($item = Helpers::getItemByNombre(Request::get('producto')))
            @else
                @php($item = null)
            @endif


            <div class="form-group col-sm-8 ">
                {{ Form::open(['action' => ['QueryController@autocompleteProducto'], 'method' => 'GET', 'id'=>'auto_producto']) }}
                {{ Form::label('producto', 'Producto:', ['class' => 'control-label']) }}
                {{ Form::text('producto',Request::get('producto'), ['id' =>  'out_producto','class' => 'form-control','placeholder' =>  'Nombre del producto', 'required']) }}
                {{ Form::close() }}
            </div>

            <meta name="csrf-token" content="{{ csrf_token() }}" />
            {!! Form::open(['url'=>'addSalida','id'=>'form_salida'])!!}
            @include('inventario/salidas/partials/_form', ['submit_text' => 'Guardar'])
            {!! Form::close() !!}

        </div>
    </div>

@stop