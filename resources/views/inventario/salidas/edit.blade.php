@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Editar salida</div>

        <div class="panel-body">

            {!! Form::model($salida, ['method' => 'PATCH', 'route' => ['salidas.update', $salida->id]]) !!}
            @include('inventario/salidas/partials/_form', ['submit_text' => 'Editar salida'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection
