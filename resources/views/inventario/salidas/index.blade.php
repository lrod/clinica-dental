@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
            <div class="panel-heading">Salidas de inventario</div>

        <div class="panel-body">
            <div class="pull-right">
                <div class="btn-group">
                    <a class="btn btn-primary" href="{{ url('inventario') }}"> Inventario</a>
                    @permission('salida-create')
                    <a class="btn btn-success" href="{{route('salidas.create') }}">Crear Salida</a>
                    @endpermission
                </div>
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                <th>Codigo</th>
                <th>Fecha</th>
                <th>Producto</th>
                <th>Cantidad</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($salidas as $salida)
                    @php($prod = \Helpers::getItemById($salida->item_id))
                    @php($fecha = new \Carbon\Carbon($salida->fecha))
                    <tr>
                        <td>{{$salida->id}}</td>
                        <td>{{$fecha->formatLocalized('%d-%m-%Y')}}</td>
                        <td><a href="{{ route('items.show', $salida->item_id,['tab'=>'']) }}">{{ $prod!==null?$prod->producto:'XXX' }}</a></td>
                        <td class="valor">{{$salida->cantidad}}</td>
                        @permission('salidas-edit')
                        <td> {!! link_to_route('salidas.edit', 'Editar', array($salida->id), array('class' => 'btn btn-default')) !!} </td>
                        @endpermission
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop