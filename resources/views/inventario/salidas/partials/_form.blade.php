<div class="form-group col-sm-12 ">
    <div class="panel panel-info">
        <div class="panel-heading" id="info-producto">

        </div>
    </div>
</div>
<div class="row vertical-align">
<div class="form-group col-sm-4 ">
    {!! Form::label('fecha', 'Fecha:', ['class' => 'control-label']) !!}
    {!! Form::date('fecha', null, ['class' => 'form-control', 'id'=>'out_fecha']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('cantidad', 'Cantidad:', ['class' => 'control-label']) !!}
    {!! Form::number('cantidad', null, ['class' => 'form-control', 'id' => 'out_cantidad']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::submit($submit_text, ['class'=>'btn btn-primary']) !!}
</div>
</div>