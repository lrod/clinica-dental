@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Items</div>

        <div class="panel-body">
            <div class="pull-right">
                <div class="btn-group">
                <a class="btn btn-primary" href="{{ url('inventario') }}"> Inventario</a>
                @permission('item-create')
                    <a class="btn btn-success" href="{{route('items.create') }}">Crear Item</a>
                @endpermission
                </div>
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                <th>Codigo</th>
                <th>Producto</th>
                <th>Cantidad</th>

                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{$item->codigo}}</td>
                        <td><a href="{{ route('items.show', $item->id,['tab'=>'']) }}">{{ $item->producto }}</a></td>
                        <td class="valor">{{$item->cantidad}}</td>
                        @permission('item-edit')
                        <td> {!! link_to_route('items.edit', 'Editar', array($item->id), array('class' => 'btn btn-default')) !!} </td>
                        @endpermission
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop