@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Crear item</div>
        <div class="panel-body">
            {!! Form::model(new App\Item, ['route' => ['items.store']]) !!}
            @include('inventario/items/partials/_form', ['submit_text' => 'Crear Item'])
            {!! Form::close() !!}
        </div>
    </div>

@stop