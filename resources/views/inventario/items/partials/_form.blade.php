<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Codigo:', ['class' => 'control-label']) !!}
    <div class="input-group">
        {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
      <!--  <span class="input-group-btn">
            <button type="button" class="btn btn-primary">Generar</button>
        </span> -->
    </div>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('producto', 'Producto:', ['class' => 'control-label']) !!}
    {!! Form::text('producto', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('minimo', 'Stock minimo:', ['class' => 'control-label']) !!}
    {!! Form::number('minimo', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('maximo', 'Stock maximo:', ['class' => 'control-label']) !!}
    {!! Form::number('maximo', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('inicial', 'Cantidad inicial:', ['class' => 'control-label']) !!}
    {!! Form::number('inicial',  null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <div class="pull-right">
        {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
    </div>
</div>