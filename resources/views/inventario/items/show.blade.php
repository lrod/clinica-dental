@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading ">
            <span class="fa fa-book fa-fw"></span>
            Informacion del item
            <div class="pull-right">
                <span class="glyphicon glyphicon-arrow-left"></span>
                {!! link_to_route('items.index', 'Regresar al listado de items') !!}
            </div>
        </div>

        <div class="panel-body">
            @if($item !== null)
                <div class="">
                    <i></i>
                </div>
                <div class="pull-left">
                    <h4>{{ $item->codigo }}</h4>
                    <h3>{{ $item->producto }}</h3>
                </div>
                <div class=" pull-right">
                    <div class="btn-group-vertical">
                        @permission('item-edit')
                        <a class="btn btn-info" href="{{route('items.edit', $item->id)}}">Editar</a>
                        @endpermission
                        @permission('item-delete')
                        {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('items.destroy', $item->id))) !!}
                        {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                        {!! Form::close() !!}
                        @endpermission
                    </div>
                </div>
        </div>
        <table class="table table-striped" width="100%">


        </table>
    </div>


    @endif

@stop