@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Editar item</div>

        <div class="panel-body">

            {!! Form::model($item, ['method' => 'PATCH', 'route' => ['items.update', $item->id]]) !!}
            @include('inventario/items/partials/_form', ['submit_text' => 'Editar item'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection
