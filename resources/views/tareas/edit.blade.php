@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Editar tarea</div>

        <div class="panel-body">

            {!! Form::model($tarea, ['method' => 'PATCH', 'route' => ['tareas.update', $tarea->id]]) !!}
            @include('tareas/partials/_form', ['submit_text' => 'Tarea item'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection
