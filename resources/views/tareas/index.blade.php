@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Tareas</div>
        <div class="panel-body">
            <p>
                {!! link_to_route('tareas.create', 'Crear Tarea', null, ['class'=>'btn btn-success' ]) !!}
            </p>

            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Tarea</th>
                    <th width="280px">Acciones</th>
                </tr>
                @foreach($tareas as $tarea)
                    <tr>
                        <td>{{ $tarea->id }}</td>
                        <td><a href="{{ route('tareas.show', $tarea->id) }}">{{ $tarea->tarea }}</a></td>
                        <td>
                            <div class="btn-group">
                                @permission('tarea-edit')
                                {!! link_to_route('tareas.edit', 'Editar', array($tarea->id), array('class' => 'btn btn-info')) !!}
                                @endpermission
                                @permission('tarea-delete')
                                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('tareas.destroy', $tarea->id))) !!}
                                {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                                {!! Form::close() !!}
                                @endpermission
                            </div>
                        </td>
                    </tr>
            @endforeach
            </table>
        </div>
    </div>

@stop
