@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading ">
            <span class="fa fa-tasks fa-fw"></span>
            Informacion de la tarea
            <div class="pull-right">
                <span class="glyphicon glyphicon-arrow-left"></span>
                {!! link_to_route('tareas.index', 'Regresar al listado de tareas') !!}
            </div>
        </div>

        <div class="panel-body">
            @if($tarea !== null)
                <div class="">
                    <i></i>
                </div>
                <div class="pull-left">
                    <h4>{{ $tarea->id }}</h4>
                    <h3>{{ $tarea->tarea }}</h3>
                    <h3>{{ $tarea->created_at }}</h3>
                    <h3>{{ $tarea->updated_at }}</h3>
                </div>
                <div class=" pull-right">
                    <div>
                        @permission('tarea-edit')
                        {!! link_to_route('tareas.edit', 'Editar', array($tarea->id), array('class' => 'btn btn-info')) !!}
                        @endpermission
                        @permission('tarea-delete')
                        {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('tareas.destroy', $tarea->id))) !!}
                        {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                        {!! Form::close() !!}
                        @endpermission
                    </div>
                </div>
        </div>
        <table class="table table-striped" width="100%">


        </table>
    </div>


    @endif

@stop