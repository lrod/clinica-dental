<div class="form-group col-sm-12">

    {!! Form::label('tarea', 'Tarea:', ['class' => 'control-label']) !!}
    {!! Form::text('tarea', null, ['class' => 'form-control']) !!}

    <div class="pull-right">
        {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
    </div>

</div>
