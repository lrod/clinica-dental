@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Crear tarea</div>
        <div class="panel-body">

            {!! Form::model(new App\Tarea, ['route' => ['tareas.store']]) !!}
            @include('tareas/partials/_form', ['submit_text' => 'Crear Tarea'])
            {!! Form::close() !!}

        </div>
    </div>

@stop