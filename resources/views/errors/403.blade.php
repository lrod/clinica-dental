<!DOCTYPE html>
<html>
    <head>
        <title>No permitido.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #312e25;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 42px;
                margin-bottom: 40px;
                color: #9e0505;
            }
            a{
                text-decoration: none;
                display: inline-block;
                margin: 0px 20px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img src="/uploads/logo.png" alt="Logo" id="logo" style="width:500px;height:80px">
                <h1 class="title">Error 403. Acceso Denegado</h1>
                <h2><strong>No estas autorizado para ver este recurso</strong></h2>
                <div>
                    <a href="{{url('/')}}"><h2>Inicio</h2></a>
                    <a href="{{ URL::previous() }}"><h2>Regresar</h2></a>
                </div>
            </div>
        </div>
    </body>
</html>
