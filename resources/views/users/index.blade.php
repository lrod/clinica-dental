@extends('layouts.app')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">Usuarios</div>
		<div class="panel-body">
			<div class="pull-right">
				{!! link_to_route('users.create', 'Crear usuario', null, array('class' => 'btn btn-info')) !!}
			</div>
			@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
			<table class="table table-bordered">
				<tr>
					<th>No</th>
					<th>Nombre</th>
					<th>Usuario</th>
					<th>Roles</th>
					<th width="280px">Accion</th>
				</tr>
				@foreach ($data as $key => $user)
					<tr>
						<td>{{ ++$i }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->user }}</td>
						<td>
							@if(!empty($user->roles))
								@foreach($user->roles as $v)
									<label class="label label-success">{{ $v->display_name }}</label>
								@endforeach
							@endif
						</td>
						<td>
							<a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Ver</a>
							<a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Editar</a>
							{!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
							{!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
							{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
			</table>
			{!! $data->render() !!}
		</div>
	</div>

@stop