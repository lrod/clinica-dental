@extends('layouts.app')

@section('content')


            <div class="panel panel-default">
                <div class="panel-heading">Editar paciente</div>

                <div class="panel-body">

			    {!! Form::model($paciente, ['method' => 'PATCH', 'route' => ['pacientes.update', $paciente->identificacion]]) !!}
			        @include('pacientes/partials/_form', ['submit_text' => 'Editar paciente'])
			    {!! Form::close() !!}

			    </div>
            </div>


@endsection
