@extends('layouts.app')

@section('content')
              <div class="panel panel-default">
                <div class="panel-heading">Crear paciente</div>

                <div class="panel-body">
				    {!! Form::model(new App\Paciente, ['route' => ['pacientes.store']]) !!}

				        @include('pacientes/partials/_form', ['submit_text' => 'Crear Paciente'])
				    {!! Form::close() !!}

    			</div>
            </div>

@endsection
