@extends('layouts.app')

@section('content')
	
	<div class="panel panel-default">
        <div class="panel-heading ">
        		<span class="glyphicon glyphicon-user"></span>
        		 Detalles del procedimiento
			<div class="pull-right">
				<span class="glyphicon glyphicon-arrow-left"></span>
				{!! link_to_route('pacientes.show', 'Regresar a la historia clinica', array($paciente->identificacion)) !!} 
		    	</div>
        </div>

        <div class="panel-body"> 


        	<div class="panel panel-info">
        		<div class="panel-heading">
        			<h3>
        			{!! link_to_route('pacientes.show', $paciente->nombreCompleto(), array($paciente->identificacion)) !!} 
        			</h3>
        		</div>
        		<div class="row">
        		
	        		<div class="col-sm-6">
	        					        		
		        		<h2>{{ $procedimiento->nombre }}</h2>

		    			<p>{{ $procedimiento->descripcion }}</p>
		    			<p>Valor: {{ $procedimiento->valor }}</p>
		    			
	        			
	        		</div>
	        		<div class="col-sm-6">
	        		
	        			@if($datos)		        		
	        			@php($date_change = new \Carbon\Carbon($datos[0]->fecha_cambio))
		    			<p>Estado: {{ $datos[0]->estado }}
		    			@if($datos[0]->estado == \Helpers::getProcedimientoEstados()[3])
		    				{{$datos[0]->fecha_cambio}} {{$date_change->diffForHumans()}}
	    				@endif
		    			</p>	
		    			<p>Iniciado: {{ $datos[0]->fecha }}  {{$datos['duracion']}}</p>	
		    			<p>Ultimo Control: {{ $datos['ultimo_control']}}  {{$datos['inactividad']}}</p>	
	        			@endif
        				
        				<div class="btn-group " role="group">
        				@if($datos[0]->estado == \Helpers::getProcedimientoEstados()[1])
                    		{!! link_to_action('ProcedimientoController@terminar', 'Terminar', array($paciente, $procedimiento), array('class' => 'btn btn-danger')) !!}
                        @elseif($datos[0]->estado == \Helpers::getProcedimientoEstados()[3])
                        	{!! link_to_action('ProcedimientoController@reanudar', 'Reanudar', array($paciente, $procedimiento), array('class' => 'btn btn-success')) !!}
                        @else
                        	{{ link_to_action('ProcedimientoController@suspender', 'Suspender', array($paciente, $procedimiento),array('class' => 'btn btn-danger')) }}
                        @endif               
                    	
                        </div>
	        		</div>


				</div>	
        	</div>		
        	



        
        </div>
  	</div>

@endsection
