@extends('layouts.app')

@section('content')

   <div class="panel panel-default">
		<div class="panel-heading">
			Pacientes
		</div>
	   <div class="panel-body">
		<div class="row">
		{!!  Form::open(['route' => 'pacientes.index', 'method' => 'GET','class' => 'navbar-form searchform', 'role' => 'search']) !!}
			<div class="form-group">
			<div class="input-group">
				<span class="input-group-btn">
					<select id="criterio" name="criterio" class="form-control">
					 	@php($criterio = Request::get('criterio'))
						<option name="1" value="name" {{$criterio=='name'?' selected':''}}>Nombres</option>
                        <option name="2" value="id" {{$criterio=='id'?' selected':''}}>Identificacion</option>
					</select>
				</span>
				{!! Form::text('buscar', Request::get('buscar'), ['required', 'class' => 'form-control', 'placeholder' => 'Buscar..']) !!}
				<span class="input-group-btn">
					{!! Form::submit('Buscar', array('class' => 'btn btn-default')) !!}
				</span>
			</div>
				@if(Request::get('buscar'))
					<span>{!! link_to_route('pacientes.index', 'Mostrar todo', null, array('class' => 'btn btn-info')) !!}</span>
				@endif
			</div>


		   <div class="pull-right">
			   	@permission('paciente-create')
				{!! link_to_route('pacientes.create', 'Crear Paciente',null,['class'=>'btn btn-success']) !!}
		   		@endpermission
		   </div>
			{!!  Form::close() !!}
		</div>
		<div class="row">
		@if(count($pacientes)===0)
			No existen pacientes registrados
		@else
			<ul class="list-group">
			@foreach($pacientes as $paciente)
				<li class="list-group-item">
					<div class="row">
						<span class="paciente-id col-sm-2">{{$paciente->identificacion}}</span>
						<span class="paciente-nombre col-sm-8">
							<a href="{{ route('pacientes.show', $paciente->identificacion,['tab'=>'']) }}">{{ $paciente->nombreCompleto() }}</a>
						</span>
					</div>
				</li>
			@endforeach
			</ul>
		@endif
		</div>
	   <?php echo $pacientes->render(); ?>
	</div> <!-- /.panel-default -->

@stop
