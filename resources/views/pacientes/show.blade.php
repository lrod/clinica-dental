@extends('layouts.app')

@section('content')


            <div class="panel panel-default">
                <div class="panel-heading ">
                		<span class="glyphicon glyphicon-user"></span>
                		 Informacion del paciente
					<div class="pull-right">
						<span class="glyphicon glyphicon-arrow-left"></span>
        				{!! link_to_route('pacientes.index', 'Regresar al listado de pacientes') !!} 
      		    	</div>
                </div>

                <div class="panel-body"> 

                @if($paciente !== null)
				<div class="pull-left">
					@if($paciente->foto!=null)
						<a href="#" data-toggle="modal"
						   data-target="#picModal"><img src="{{asset('/uploads/profiles/'.$paciente->foto)}}" class="img-circle" style="width:80px;height:80px"/></a>
					@else
						<a href="#" data-toggle="modal"
						   data-target="#picModal">
						<img src="{{asset('/uploads/default_profile.jpg')}}" class="img-circle" style="width:80px;height:80px"/></a>
					@endif
							<!-- Modal -->
						<div class="modal fade" id="picModal" tabindex="-1" role="dialog"
							 aria-labelledby="picModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4>Cambiar foto de perfil</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="pull-left">
												<img src="{{asset($paciente->foto==null?'/uploads/default_profile.jpg':'/uploads/profiles/'.$paciente->foto)}}" class="img" style="width:250px;height:250px"/>
											</div>
											<div class="pull-right">
												{!! Form::open(['action'=>['ApplyController@uploadProfilePicture', 'identificacion'=>$paciente->identificacion], 'method'=>'POST','files'=>'true','class'=>'form-inline']) !!}
												{!! Form::file('archivo') !!}<br>
												<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
												{!! Form::submit('Guardar', ['class'=>'btn primary']) !!}
												{!! Form::close() !!}
											</div>
										</div>

									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal -->
						</div>
				</div>
				<div class="pull-left">
					<h4>{{ $paciente->identificacion }}</h4>
					<h3>{{ $paciente->nombreCompleto() }}</h3>
				</div>
					<div class="btn-group pull-right" role="group">

						@permission('cita-create')
						{!! link_to_route('citas.create', 'Agendar cita', ['buscar'=>$paciente->identificacion], ['class'=>'btn btn-primary', 'id'=>'1234']) !!}
						@endpermission
						@permission('paciente-edit')
						{!! link_to_route('pacientes.edit', 'Editar', array($paciente->identificacion), array('class' => 'btn btn-info')) !!}
						@endpermission
					</div>

				<table class="table table-striped" width="100%">
					<tr>
						<td>Fecha nacimiento:</td>
						<td>{{ $paciente->fecha_nacimiento }} </td>
						<td>Edad:</td>
						<td>{{ $paciente->edad() }}</td>
					</tr>
					<tr>
						<td>Dirección:</td>
						<td>{{ $paciente->direccion }}</td>
						<td>Telefono:</td>
						<td>{{ $paciente->telefono1 }}</td>
					</tr>
					<tr>
						<td>Correo:</td>
						<td>{{ $paciente->email }}</td>
						<td>Ocupacion:</td>
						<td>{{ $paciente->ocupacion }}</td>
					</tr>					
				</table>

				@else
					<h2>Usuario no encontrado</h2>
				@endif


				<div class="tabbable">
				    <ul class="nav nav-tabs" id="myTabs" role="tablist">    
				        <li class="active" role="presentation">
				        	<a href="#historia"  role="tab" data-toggle="tab">Historia clinica</a>
				        </li>
						@if($paciente->historia !== null)
							<li role="presentation">
								<a href="#examen_facial" role="tab" data-toggle="tab">Examen Facial</a>
							</li>
							<li role="presentation">
								<a href="#examen_dental" role="tab" data-toggle="tab">Examen Dental</a>
							</li>
							<li role="presentation">
								<a href="#odontograma" role="tab" data-toggle="tab">Odontograma</a>
							</li>
							<li role="presentation">
								<a href="#ayudas_diagnosticas" role="tab" data-toggle="tab">Ayudas Diagnosticas</a>
							</li>
							<li role="presentation">
								<a href="#procedimientos" role="tab" data-toggle="tab">Procedimientos</a>
							</li>
							<li role="presentation">
								<a href="#avances" role="tab" data-toggle="tab">Progreso</a>
							</li>
							<li role="presentation">
								<a href="#agenda" role="tab" data-toggle="tab">Controles</a>
							</li>
							<li role="presentation">
								<a href="#cartera" role="tab" data-toggle="tab">Cartera</a>
							</li>
						@endif
				    </ul>
				    <div class="tab-content">
				        <div class="tab-pane active" id="historia">
							@if($paciente->historia !== null)
								@include('historias/partials/_show', ['historia' => $paciente->historia])
							@else

								{!! Form::model(new App\Historia, ['route' => ['pacientes.historias.store', $paciente->identificacion]]) !!}
								@include('historias/partials/_form', ['submit_text' => 'Crear Historia'])
								{!! Form::close() !!}
							@endif
				        </div>
						@if($paciente->historia !== null)
							<div class="tab-pane" id="examen_facial">
								@if($paciente->historia->examenfacial !== null)
									@include('historias.partials._showfacial', ['examenfacial' => $paciente->historia->examenfacial])
								@else
									{!! Form::model(new App\ExamenFacial, ['route' => ['pacientes.historias.examenfacial.store', $paciente->identificacion, $paciente->historia->id], 'class'=>'form-horizontal']) !!}
									@include('historias.partials._facial', ['submit_text'=>'Guardar'])
									{!! Form::close() !!}
								@endif
							</div>
							<div class="tab-pane" id="examen_dental">
								@if($paciente->historia->examendental !== null)
									@include('historias.partials._showdental', ['examendental' => $paciente->historia->examendental])
								@else
									{!! Form::model(new App\ExamenDental, ['route' => ['pacientes.historias.examendental.store', $paciente->identificacion, $paciente->historia->id], 'class'=>'form-horizontal']) !!}
									@include('historias.partials._dental', ['submit_text'=>'Guardar'])
									{!! Form::close() !!}
								@endif
							</div>
							<div class="tab-pane" id="odontograma">
								<div class="col-sm-6">
									{!! Form::open(['action'=>['ApplyController@upload'], 'method'=>'POST','files'=>'true','class'=>'form-inline']) !!}
									@include('historias.partials._odontograma', ['paciente' => $paciente, 'tipo'=>'inicial'])
									{!! Form::close() !!}
									@if($paciente->historia->odontogramas !== null)
										<section>
											<img src={{'/uploads/odontogramas/'.$paciente->historia->odontogramas->inicial}} alt="" class="img-responsive">
										</section>
									@else
										<span>No imagen</span>
									@endif
								</div>
								<div class="col-sm-6">
									{!! Form::open(['action'=>['ApplyController@upload'], 'method'=>'POST','files'=>'true','class'=>'form-inline']) !!}
									@include('historias.partials._odontograma', ['paciente' => $paciente, 'tipo'=>'final'])
									{!! Form::close() !!}
									@if($paciente->historia->odontogramas !== null)
										<section>
											<img src={{'/uploads/odontogramas/'.$paciente->historia->odontogramas->final}} alt="" class="img-responsive">
										</section>
									@else
										<span>No imagen</span>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="ayudas_diagnosticas">
								<div class="row">
								{!! Form::open(['action'=>['ApplyController@uploadDiagnostica'], 'method'=>'POST','files'=>'true','class'=>'form-inline']) !!}
								@include('historias.partials._diagnosticas', ['historia' => $paciente->historia])
								{!! Form::close() !!}
								</div>
								<div class="row">
									@if($paciente->historia->diagnosticas)
										@foreach($paciente->historia->diagnosticas as $diagnostica)
											<div class="col-sm-6 col-md-4">
												<div class="thumbnail">
													<img src="{{'/uploads/diagnosticas/'.$diagnostica->archivo}}" alt=""style="width:250px;height:250px">
													<div class="caption">
														<h4>{{$paciente->historia->getDiagnosticas()[$diagnostica->tipo]}}</h4>
														<h5>{{$diagnostica->fecha}}</h5>
													</div>
												</div>
											</div>
										@endforeach
									@else
										<span>No se han cargado ayudas diagnosticas</span>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="procedimientos">
								<div class="row">
								<div class="col-sm-6">
								{!! Form::open(['action' => ['ControlesController@addProcedimiento',
								$paciente->identificacion],'method'=>'POST']) !!}
								@include('historias.partials._list', ['paciente'=>$paciente])
								{!! Form::close() !!}

								</div>
								<div class="panel panel-default col-sm-6">
									<div class="panel-heading">Listado de procedimientos</div>
									<div class="panel-body">
								@if(count($paciente->procedimientos)===0)
									El paciente no tiene procedimientos aplicados
								@else
									<ul class="list-group">
										@foreach($paciente->procedimientos as $procedimiento)
											<li class="list-group-item">
												<div class="row">
												<div class="col-sm-10">	
													<span>{{$procedimiento->servicio->nombre}}</span><br>
													<span>{{$procedimiento->nombre}}</span>
													<span>{{$procedimiento->valor}}</span>
												</div>
												<div class="col-sm-2 pull-right">
												{!! link_to_route('pacientes.detail', 'Ver', array($paciente, $procedimiento), ['class'=>'btn btn-primary', 'id'=>'1234']) !!}
												</div>
												</div>		
											</li>
										@endforeach
									</ul>
								@endif
									</div>
								</div>
								</div>
							</div>
							<div class="tab-pane" id="avances">
								<div class="row">
								{!! Form::open(['action'=>['ApplyController@uploadAvance'], 'method'=>'POST','files'=>'true','class'=>'form-inline']) !!}
								@include('historias.partials._avances', ['historia' => $paciente->historia])
								{!! Form::close() !!}
								</div>
								<div class="row">
									@if($paciente->historia->avances)
										@foreach($paciente->historia->avances as $avance)
											<div class="col-sm-6 col-md-4">
												<div class="thumbnail">
													<img src="{{'/uploads/avances/'.$avance->archivo}}" alt=""style="width:250px;height:250px">
													<div class="caption">
														<h5>{{$avance->fecha}}</h5>
														<h5>{{$avance->descripcion}}</h5>
													</div>
												</div>
											</div>
										@endforeach
									@else
										<span>No se han cargado avances del procedimiento</span>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="agenda">

								<!-- Button trigger modal -->
								<button class="btn btn-primary" data-toggle="modal"
										data-target="#myModal">
									Agregar control
								</button>
								<!-- Modal -->
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
									 aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
													&times;
												</button>
												<h4 class="modal-title" id="myModalLabel">
													Agregar control
												</h4>
											</div>
											<div class="modal-body">
												{!! Form::model(new App\Control, ['route' => ['pacientes.controles.store', $paciente->identificacion], 'class'=>'form-horizontal']) !!}
												@include('historias.partials._control', ['submit_text'=>'Guardar'])

												<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
												{!! Form::submit('Guardar', ['class'=>'btn primary']) !!}
												{!! Form::close() !!}
											</div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal -->
								</div>

								@if(count($paciente->controles)===0)
									El paciente no tiene controles aplicados
								@else
									<table class="table table-striped">
										<thead>
										<th>Control</th>
										<th>Fecha</th>
										<th>Trabajo realizado</th>
										<th>Trabajo pendiente</th>
										</thead>
									<tbody>
										@foreach($paciente->controles as $control)
											<tr>
												<td>{{$control->id}}</td>
												<td>{{$control->fecha}}</td>
												<td>{{$control->trabajo_realizado}}</td>
												<td>{{$control->trabajo_pendiente}}</td>
											</tr>
										@endforeach
									</tbody>
									</table>
								@endif
							</div>

							<div class="tab-pane" id="cartera">
							<!--	<p class="btn btn-default">
									{{-- link_to_asset('#', 'Facturar') --}}
								</p>  -->

								<table  id="tabla-factura" class="table table-bordered" width="280px">
									<tr>
										<td>Total procedimientos:</td>
										<td class="text-right">{{Helpers::getTotalProcedimientos($paciente->procedimientos)}}</td>
									</tr>
									<tr>
										<td>Cuota inicial:</td>
										<td class="text-right">{{Helpers::getTotalCuotaInicial($paciente->id)}}</td>
									</tr>
									<tr>
										<td>Total abonos:</td>
										<td class="text-right">{{Helpers::getTotalAbonos($paciente->controles)}}</td>
									</tr>
									<tr>
										<td>Saldo:</td>
										<td class="text-right">{{Helpers::getSaldo($paciente->procedimientos, $paciente->controles, $paciente->id)}}</td>
									</tr>
								</table>

							</div>

						@endif
					</div>
				</div>
				
				<hr>
				
			</div>
            </div>            

	
@stop