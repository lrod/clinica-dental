@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Servicios</div>
        <div class="panel-body">

            <h2>Crear procedimiento para el servicio de {{ $servicio->nombre }}</h2>
            {!! Form::model(new App\Procedimiento, ['route' => ['servicios.procedimientos.store', $servicio->id], 'class'=>'form-horizontal']) !!}
            @include('procedimientos/partials/_form', ['submit_text' => 'Crear Procedimiento'])
            {!! Form::close() !!}
        </div>
    </div>

@endsection
