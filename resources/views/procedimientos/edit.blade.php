@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Servicios</div>
        <div class="panel-body">

            <h2>Editar procedimiento "{{ $procedimiento->nombre }}"</h2>

            {!! Form::model($procedimiento, ['method' => 'PATCH', 'route' => ['servicios.procedimientos.update', $servicio->id, $procedimiento->id], 'class' => 'form-horizontal']) !!}
            @include('procedimientos/partials/_form', ['submit_text' => 'Editar Procedimiento'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection
