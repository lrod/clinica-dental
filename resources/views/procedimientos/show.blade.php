@extends('layouts.app')

@section('content')

    <h2>
        {!! link_to_route('servicios.show', $servicio->nombre, [$servicio->id]) !!} -
        {{ $procedimiento->nombre }}
    </h2>

    <p>{{ $procedimiento->descripcion }}</p>
    <p>Valor: {{ $procedimiento->valor }}</p>

@endsection
