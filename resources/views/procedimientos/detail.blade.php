@extends('layouts.app')

@section('content')
	
	<h1>Detalles del procedimiento</h1>
	
	<h2>{{$paciente->nombreCompleto()}}</h2>

    <h2>        
        {{ $procedimiento->nombre }}
    </h2>

    <p>{{ $procedimiento->descripcion }}</p>
    <p>Valor: {{ $procedimiento->valor }}</p>

@endsection
