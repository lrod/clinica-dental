@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Roles</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Gestion de Roles</h2>
                    </div>
                    <div class="pull-right">
                        @permission('role-create')
                        <a class="btn btn-success" href="{{ route('roles.create') }}"> Crear Nuevo Rol</a>
                        @endpermission
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th width="280px">Accion</th>
                </tr>
                @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td>{{ $role->description }}</td>
                        <td>
                            <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Ver</a>
                            @permission('role-edit')
                            <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Editar</a>
                            @endpermission
                            @permission('role-delete')
                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                            @endpermission
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $roles->render() !!}
        </div>
    </div>

@stop