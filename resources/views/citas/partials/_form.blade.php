


{!! Form::hidden('paciente_id', count($pacientes)?$pacientes->first()->id:0)  !!}
{!! Form::hidden('estado', Helpers::getCitaEstados()[2])  !!}

<div class="col-sm-12 ">
    <div class="form-group">
        {!! Form::label('paciente', 'Paciente:', ['class' => 'control-label']) !!}
        {!! Form::text('nombre', count($pacientes)?$pacientes->first()->nombreCompleto():'', ['class' => 'form-control', 'readonly']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('medico_id', 'Medico:', ['class' => 'control-label']) !!}
        {!! Form::select('medico_id', $medicos, null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fecha', 'Fecha:', null, ['class' => 'control-label']) !!}
        {!! Form::date('fecha',null, ['class' => 'control-label', 'id'=>'datepicker']) !!}
        {!! Form::label('tiempo',Helpers::getFecha(Input::get('fecha')),['class'=>'control-label','id'=>'tiempo']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('hora', 'Horario 1:', ['class' => 'control-label']) !!}
        <div class="input-group">
        <div class="input-group-addon" id="jornada1">
        </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('hora', 'Horario 2:', ['class' => 'control-label']) !!}
        <div class="input-group">
        <div class="input-group-addon" id="jornada2">
        </div>
        </div>
    </div>
    <ul id="lista">

    </ul>
</div>
<div class="form-group">
    {!! Form::submit($submit_text) !!}
</div>