{!! Form::hidden('paciente_id', $cita->paciente->id)  !!}
{!! Form::hidden('medico_id', $cita->medico->id)  !!}
{!! Form::hidden('estado', Helpers::getCitaEstados()[3])  !!}
<div class="col-sm-12 ">
    <div class="form-group">
        {!! Form::label('paciente', 'Paciente:', null, ['class' => 'control-label']) !!}
        {{$cita->paciente->nombreCompleto()}}

    </div>
    <div class="form-group">
        {!! Form::label('medico', 'Medico:', null, ['class' => 'control-label']) !!}
        {{$cita->medico->nombreCompleto()}}

    </div>
    <div class="form-group">
        {!! Form::label('programada', 'Programada para:', null, ['class' => 'control-label']) !!}
        <span>{{$cita->fecha}}</span> <span>{{$cita->hora}}</span>

    </div>
    <div>Escoja una fecha y hora para reprogramar la cita</div>
    <div class="form-group">
        {!! Form::label('fecha', 'Fecha:', null, ['class' => 'control-label']) !!}
        {!! Form::date('fecha',null, ['class' => 'control-label', 'id'=>'datepicker']) !!}
        {!! Form::label('tiempo','',['class'=>'control-label','id'=>'tiempo']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('hora', 'Hora:', ['class' => 'control-label']) !!}
        <div class="input-group">
            <div class="input-group-addon" id="jornada1">
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('hora', 'Hora:', ['class' => 'control-label']) !!}
        <div class="input-group">
            <div class="input-group-addon" id="jornada2">
            </div>
        </div>
    </div>
    <ul id="lista">

    </ul>
</div>
<div class="form-group">
    {!! Form::submit($submit_text) !!}
</div>