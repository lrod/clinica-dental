@extends('layouts.app')

@section('content')
    <h2>Crear cita </h2>

    @if(Request::has('buscar'))
        @php($pacientes = Helpers::getPacienteByIdentificacion(Request::get('buscar')))
        @php($medicos = DB::table('medicos')->where('habilitado','=',1)->lists('apellido1', 'id'))
    @endif

    {!! Form::open(['method'=>'GET','url'=> action('QueryController@search'), 'class' => 'navbar-form searchform', 'role' => 'search']) !!}

        <div class="input-group">
            <span class="input-group-btn">
                <select id="criterio" name="criterio" class="form-control">
                    <option name="1" value="id">Identificacion</option>
                </select>
                {!! Form::number('buscar',Request::get('buscar'), ['class' => 'form-control', 'required']) !!}

            </span>
            <span class="input-group-btn">

                {{ Form::button('<i class="fa fa-search"></i> Buscar', array('type' => 'submit', 'class' => 'btn btn-default')) }}
            </span>
        </div>
    {!! Form::close() !!}



@if($msg1!='')
    <div class='flash alert-danger'>
       {{ $msg1}}
        </div>
    @else
        <div class='flash alert-danger'>
            All right
        </div>
    @endif


    {!! Form::model(new App\Cita, ['route' => ['citas.store'], 'class'=>'form-horizontal']) !!}
    @include('citas/partials/_form',
        ['submit_text' => 'Crear Cita',
         'pacientes'=>$pacientes,
         'medicos'=>$medicos
         ])
    {!! Form::close() !!}
@endsection