@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="fa fa-calendar-check-o"></span>Citas
            <div class="pull-right">
                <span class="glyphicon glyphicon-arrow-left"></span>
                {!! link_to_asset('home', 'Regresar a la agenda') !!} 
            </div>
        </div>

        <div class="panel-body">
             <div class="row ">
                <div class="col-sm-6 pull-left">
                    <div class="row">
                    <span class="col-xs-6">Fecha: <span>{{$cita->fecha}}</span></span>
                    <span class="col-xs-6">Hora: <span>{{$cita->hora}}</span></span>
                    <span class="col-xs-12">Paciente: <a href="{{ route('pacientes.show', $cita->paciente->identificacion,['tab'=>'']) }}"><span>{{ $cita->paciente->nombreCompleto() }}</span></a></span>
                    <span class="col-xs-12">Medico: <span>{{$cita->medico->nombreCompleto()}}</span></span>
                    </div>
                </div>
                <div class="col-sm-6 pull-right">
                    <div id="estado">
                       <span>{{$cita->estado}}</span>
                    </div>
                    @if($cita->estado != Helpers::getCitaEstados()[4] && $cita->estado != Helpers::getCitaEstados()[1])
                    <div class="btn-group pull-right" role="group">
                        @permission('cita-edit')
                        {!! link_to_action('CitaController@atender', 'Atender', array($cita->id, 1), array('class' => 'btn btn-success')) !!}
                        @endpermission
                        @permission('cita-edit')
                        {!! link_to_route('citas.edit', 'Reprogramar', array($cita->id, 1), array('class' => 'btn btn-info')) !!}
                        @endpermission
                        @permission('cita-edit')
                        {!! link_to_action('CitaController@cancelar', 'Cancelar', array($cita->id, 1),array('class' => 'btn btn-danger')) !!}
                        @endpermission
                    </div>
                    @endif

                </div>
                </div>
        </div>
    </div>

@stop
