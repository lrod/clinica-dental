@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Editar cita</div>

        <div class="panel-body">

            {!! Form::model($cita, ['method' => 'PATCH', 'route' => ['citas.update', $cita->id], 'class' => 'form-horizontal']) !!}
            @include('citas/partials/_form1', ['submit_text' => 'Editar cita'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection
