@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Citas</div>

        <div class="panel-body">
            <div class="row">
            {!!  Form::open(['route' => 'citas.index', 'method' => 'GET','class' => 'navbar-form searchform', 'role' => 'search']) !!}
            <div class="form-group pull-left">
                <div class="input-group">
                <span class="input-group-btn">
                    <select id="filtro-cita" name="filtro-cita" class="form-control">
                        @php($filtrocita = Request::get('filtro-cita'))
                        <option name="1" value="estado" {{$filtrocita=='estado'?' selected':''}}>Estado</option>
                        <option name="2" value="medico" {{$filtrocita=='medico'?' selected':''}}>Medico</option>
                    </select>
                </span>
                    {{-- Form::text('buscar', Request::get('buscar'), ['required', 'class' => 'form-control', 'placeholder' => 'Buscar..']) --}}

                    <select name="filtro" id="filtro" class="form-control">
                        @php($filtro = Request::get('filtro'))
                        @if($filtrocita=='estado' || empty($filtrocita))
                            <option disabled {{$filtro==''?' selected':''}}>Seleccione..</option>
                            <option value="Pendiente" {{$filtro=='Pendiente'?' selected':''}}>Pendiente</option>
                            <option value="Atendida" {{$filtro=='Atendida'?' selected':''}}>Atendida</option>
                            <option value="Cancelada" {{$filtro=='Cancelada'?' selected':''}}>Cancelada</option>
                        @elseif($filtrocita=='medico')
                            @php($lmedicos = Helpers::getMedicosHabilitados())

                            <option disabled {{$filtro ==''?' selected':''}}>Seleccione</option>
                            @foreach($lmedicos as $medico)
                                <option value="{{$medico->id}}" {{$filtro==$medico->id?' selected':''}}> {{$medico->apellido1}} </option>
                            @endforeach
                        @endif
                    </select>
                    <span class="input-group-btn">
                    {!! Form::submit('Buscar', array('class' => 'btn btn-default')) !!}
                </span>
                </div>
                @if(Request::get('filtro-cita'))
                    <span>{!! link_to_route('citas.index', 'Mostrar todo', null, array('class' => 'btn btn-info')) !!}</span>
                @endif
                </div>
                <div class="pull-right">
                    @permission('cita-create')
                    {!! link_to_route('citas.create', 'Asignar cita',null,['class'=>'btn btn-primary']) !!}
                    @endpermission
                </div>
                {!!  Form::close() !!}
            </div>

            <div id="lista-citas" class="row">
                @if(count($citas)===0)
                    No existen citas registrados
                @else
                    <ul class="list-group">
                        @foreach($citas as $cita)
                            <li class="list-group-item">
                                <div class="row ">
                                <div class="col-sm-6 pull-left">
                                    <div class="row">
                                    <span class="col-xs-6">Fecha: <span>{{$cita->fecha}}</span></span>
                                    <span class="col-xs-6">Hora: <span>{{$cita->hora}}</span></span>
                                    <span class="col-xs-12">Paciente: <a href="{{ route('pacientes.show', $cita->paciente->identificacion,['tab'=>'']) }}"><span>{{ $cita->paciente->nombreCompleto() }}</span></a></span>
                                    <span class="col-xs-12">Medico: <span>{{$cita->medico->nombreCompleto()}}</span></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-right">
                                    <div id="estado">
                                       <span>{{$cita->estado}}</span>
                                    </div>
                                    @if($cita->estado != Helpers::getCitaEstados()[4] && $cita->estado != Helpers::getCitaEstados()[1])
                                    <div class="btn-group pull-right" role="group">
                                        @permission('cita-edit')
                                        {!! link_to_action('CitaController@atender', 'Atender', array($cita->id), array('class' => 'btn btn-success')) !!}
                                        @endpermission
                                        @permission('cita-edit')
                                        {!! link_to_route('citas.edit', 'Reprogramar', array($cita->id), array('class' => 'btn btn-info')) !!}
                                        @endpermission
                                        @permission('cita-edit')
                                        {!! link_to_action('CitaController@cancelar', 'Cancelar', array($cita->id),array('class' => 'btn btn-danger')) !!}
                                        @endpermission
                                    </div>
                                    @endif

                                </div>
                                </div>

                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <?php echo $citas->render(); ?>
        </div>

    </div>

@stop
