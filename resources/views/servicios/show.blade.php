@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Servicios</div>
        <div class="panel-body">
            <h2>{{ $servicio->nombre }}</h2>
            <p>
                {!! link_to_route('servicios.index', 'Regresar a servicios', null,['class'=>'btn btn-success']) !!}
                {!! link_to_route('servicios.procedimientos.create', 'Crear procedimiento', $servicio->id, ['class'=>'btn btn-success']) !!}
            </p>
            @if ( !$servicio->procedimientos->count() )
                El servicio no tiene  procedimientos.
            @else
                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th width="280px">Acciones</th>
                    </tr>
                    @foreach( $servicio->procedimientos as $procedimiento )
                    <tr>
                    <td>{{ $procedimiento->id }}</td>
                    <td><a href="{{ route('servicios.procedimientos.show', [$servicio->id, $procedimiento->id]) }}">{{ $procedimiento->nombre }}</a></td>
                    <td>
                        <div class="btn-group">
                            @permission('procedimiento-delete')
                            {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('servicios.procedimientos.destroy', $servicio->id, $procedimiento->id))) !!}
                            @permission('procedimiento-edit')
                            {!! link_to_route('servicios.procedimientos.edit', 'Editar', array($servicio->id, $procedimiento->id), array('class' => 'btn btn-info')) !!},
                            @endpermission
                            {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                            {!! Form::close() !!}
                            @endpermission
                        </div>
                    </td>
                    </tr>
                    @endforeach
                </table>
            @endif




            </div>
    </div>
@endsection
