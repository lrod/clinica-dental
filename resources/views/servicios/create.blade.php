@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Servicios</div>
        <div class="panel-body">
            <h2>Crear Servicio</h2>

            {!! Form::model(new App\Servicio, ['route' => ['servicios.store']]) !!}
            @include('servicios/partials/_form', ['submit_text' => 'Crear Servicio'])
            {!! Form::close() !!}

        </div>
    </div>

@endsection