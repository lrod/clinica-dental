@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Servicios</div>
        <div class="panel-body">
            <p>
                {!! link_to_route('servicios.create', 'Crear servicio', null, ['class'=>'btn btn-success' ]) !!}
            </p>
            @if ( !$servicios->count() )
                No hay servicios registrados
            @else
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Nombre</th>
                        <th width="280px">Acciones</th>
                    </tr>
                    @foreach( $servicios as $servicio )
                        <tr>
                            <td>{{ $servicio->id }}</td>
                            <td><a href="{{ route('servicios.show', $servicio->id) }}">{{ $servicio->nombre }}</a></td>
                            <td>
                            <div class="btn-group">

                                @permission('servicio-delete')
                                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('servicios.destroy', $servicio->id))) !!}
                                @permission('servicio-edit')
                                {!! link_to_route('servicios.edit', 'Editar', array($servicio->id), array('class' => 'btn btn-info')) !!}
                                @endpermission
                                {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                                {!! Form::close() !!}
                                @endpermission
                            </div>
                            </td>

                        </tr>
                    @endforeach
                </table>
            @endif



        </div>
    </div>

@stop
