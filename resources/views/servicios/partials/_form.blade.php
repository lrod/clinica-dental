<div class="form-group">
    {!! Form::label('nombre', 'Nombre:', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-6">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>
</div>
{!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
