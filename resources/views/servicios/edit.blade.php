@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Servicios</div>
        <div class="panel-body">
            <h2>Editar Servicio</h2>

            {!! Form::model($servicio, ['method' => 'PATCH', 'route' => ['servicios.update', $servicio->id]]) !!}
            @include('servicios/partials/_form', ['submit_text' => 'Editar servicio'])
            {!! Form::close() !!}
        </div>
    </div>

@endsection