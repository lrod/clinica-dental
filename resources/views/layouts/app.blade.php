<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Clinica</title>

    <!-- Fonts 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
|||||-->
    <!-- Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/sb-admin.css">
    <!-- MetisMenu CSS -->
    <link rel="stylesheet" href="/css/metisMenu.min.css" >

    <!-- Custom Fonts -->
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <!--<link href="css/app.css" rel="stylesheet"> -->

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">

<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href={{url('/home')}}><img src="/uploads/logo.png" alt="Logo" id="logo" style="width:240px;height:38px"></a>

    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        @if (!Auth::guest())
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> {{Auth::user()->name}}
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href={{ url('logout')}}><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        @else
            <li class="dropdown">
            <a class="dropdown-toggle"  href="{{url('/login')}}"> Login
                <i class="fa fa-user fa-fw"></i>
            </a>
                </li>
        @endif
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->



        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{url('/home')}}"><i class="fa fa-h-square fa-fw"></i> Agenda</a>
                    </li>
                    <li>
                        <a href="{{url('/pacientes')}}"><i class="fa fa-user fa-fw"></i> Pacientes</a>
                    </li>
                    <li>
                        <a href="{{url('/citas')}}"><i class="fa fa-calendar-check-o fa-fw"></i> Citas</a>
                    </li>
                    <li>
                        <a href="{{url('/medicos')}}"><i class="fa fa-user-md fa-fw"></i> Medicos</a>
                    </li>
                    <li>
                        <a href="{{url('/inventario')}}"><i class="fa fa-book fa-fw"></i> Inventario</a>
                    </li>


                    @if (!Auth::guest() && Auth::User()->hasRole('admin'))
                    <li>
                        <a href="{{url('/admin')}}"><i class="fa fa-files-o fa-fw"></i> Admin<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ route('users.index') }}">Usuarios</a>
                            </li>
                            <li>
                                <a href="{{ route('roles.index') }}">Roles</a>
                            </li>
                            <li>
                                <a href="{{url('/servicios')}}">Servicios</a>
                            </li>
                            <li>
                                <a href="{{url('/reportemensual')}}">Reporte Mensual</a>
                            </li>
                            <li>
                                <a href="{{url('/reportes')}}">Reportes</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    @endif
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>

    <!-- /.navbar-static-side -->
</nav>


    <div id="page-wrapper">

            @if (Session::has('message'))
                <div class="flash alert-info">
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif

            @if ($errors->any())
                <div class='flash alert-danger'>
                    @foreach ( $errors->all() as $error )
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
    </div>
</div>

    <!-- JavaScripts -->

    <!-- Metis Menu Plugin JavaScript -->
    {!!  Html::script('/js/app.js') !!}
    {!!  Html::script('/js/jquery-ui.min.js') !!}

    <script src="/js/sb-admin-2.js" ></script>
    <script src="/js/metisMenu.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
