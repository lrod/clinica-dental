<h3>Examen Dental</h3>

<table>
    <tr>
        <td>Oclusión molar derecha:</td>
        <td>{{$examendental->oclusion_molar_derecha}}</td>
    <tr>
        <td>Oclusión molar izquierda:</td>
        <td>{{$examendental->oclusion_molar_izquierda}}</td>
    </tr>
    <tr>
        <td>Relación canina derecha:</td>
        <td>{{$examendental->relacion_canina_derecha}}</td>
    <tr>
        <td>Relación canina izquierda:</td>
        <td>{{$examendental->relacion_canina_izquierda}}</td>
    </tr>
    <tr>
        <td>Sobremordida vertical:</td>
        <td>{{$examendental->sobremordida_vertical}}</td>
    <tr>
        <td>Sobremordida horizontal:</td>
        <td>{{$examendental->sobremordida_horizontal}}</td>
    </tr>
    <tr>
        <td>Mordida cruzada anterior:</td>
        <td>{{$examendental->mordida_cruzada_anterior}}</td>
     <tr>
        <td>Mordida cruzada posterior:</td>
        <td>{{$examendental->mordida_cruzada_posterior}}</td>
    </tr>
    <tr>
        <td>Alteracion de la erupción dentaria:</td>
        <td>{{$examendental->alteracion_erupcion_dentaria}}</td>
    </tr>
    <tr>
        <td>Examen ATM:</td>
        <td>{{$examendental->examen_atm}}</td>
    </tr>
    <tr>
        <td>Habitos:</td>
        <td>{{$examendental->habitos}}</td>
    </tr>
</table>