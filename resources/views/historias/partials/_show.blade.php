<h3>Historia clinica</h3>

<table>
    <tr>
        <td>Estado de Salud</td>
        <td>{{$historia->estado_salud}}</td>
    <tr>
        <td>Recibe atencion medica</td>
        <td>{{$historia->opc_atencion?'Si':'No'}}</td>
    </tr>
    <tr>
        <td>Enfermedades prolongadas o debilitantes</td>
        <td>{{$historia->opc_enfermedad?'Si':'No'}}</td>
        <td>{{$historia->enfermedad}}</td>
    </tr>
        <td>Toma algun medicamento</td>
        <td>{{$historia->opc_medicamentos?'Si':'No'}}</td>
        <td>{{$historia->medicamentos}}</td>
    </tr>
    <tr>
        <td>Presenta reacciones alergicas</td>
        <td>{{$historia->opc_alergias?'Si':'No'}}</td>
        <td>{{$historia->alergias}}</td>
    </tr>
    <tr>
        <td>Dolor o sensibilidad en alguna pieza dentaria</td>
        <td>{{$historia->opc_dolor?'Si':'No'}}</td>
        <td>{{$historia->dolor}}</td>
    </tr>
    <tr>
        <td>Le han extraido piezas dentarias</td>
        <td>{{$historia->opc_extraido?'Si':'No'}}</td>
        <td>{{$historia->extraido}}</td>
    </tr>
    <tr>
        <td>Ha recibido anteriormente tratamiento de ortodoncia</td>
        <td>{{$historia->opc_ortodoncia?'Si':'No'}}</td>
    </tr>
</table>