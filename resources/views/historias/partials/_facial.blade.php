
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('frente', 'Frente:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_frente', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('perfil', 'Perfil:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_perfil', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('relacion_labios', 'Relacion labios:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_relacion_labios', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('sonrisa', 'Sonrisa:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_sonrisa', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('incisivos', 'Visualizacion de incisivos:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_incisivos', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('arco_sonrisa', 'Arco de la sonrisa:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_arco_sonrisa', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('contorno_gingival', 'Contorno gingival:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_contorno_gingival', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('linea_media', 'Linea media:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_linea_media', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('linea_superior', 'Linea superior:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_linea_sup', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('linea_inferior', 'Linea inferior:', ['class' => 'control-label']) !!}
    {!! Form::text('fac_linea_inf', null, ['class' => 'form-control']) !!}
</div>
</div>

{!! Form::submit($submit_text, ['class'=>'btn primary']) !!}