{!! Form::hidden('paciente_id', $paciente->id) !!}
<div class=col-sm-12>
    <div class="form-group">
        {!! Form::label('servicio', 'Servicio:', ['class' => 'control-label']) !!}
        {!! Form::select('servicio', ['-1'=>'Seleccione']+$servicios->toArray(), null, ['id'=>'servicio','class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-12>
    <div class="form-group">
        {!! Form::label('procedimiento', 'Procedimiento:', ['class' => 'control-label']) !!}
        {!! Form::select('procedimiento', ['-1'=>'Seleccione'],null,['id'=>'procedimiento', 'class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('valor_proc', 'Valor:', ['class' => 'control-label']) !!}
        {!! Form::text('valor_proc', null, ['class' => 'form-control', 'readonly']) !!}
    </div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('cuota_inicial', 'Cuota inicial:', ['class' => 'control-label']) !!}
        {!! Form::number('cuota_inicial', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-12>
    <div class="form-group">
        {!! Form::label('fecha', 'Fecha de inicio:', ['class' => 'control-label']) !!}
        {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-12">
    {!! Form::submit('Agregar', ['class'=>'btn primary']) !!}
</div>