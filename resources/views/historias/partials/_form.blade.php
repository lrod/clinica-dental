<div class="form-group col-sm-12">
    {!! Form::label('estado_salud', 'Estado de salud:', ['class' => 'control-label']) !!}
    {!! Form::text('estado_salud', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('atencion_medica', '¿Recibe atencion medica?', ['class' => 'control-label']) !!}
    <div class="input-group">
        <label class="radio-inline">
            {{Form::radio('opc_atencion','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_atencion','0')}}NO
        </label>
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('ortodoncia_previa', 'Ha recibido anteriormente tratamiento de ortodoncia:', ['class' => 'control-label']) !!}
    <div class="input-group">
        <label class="radio-inline">
            {{Form::radio('opc_ortodoncia','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_ortodoncia','0')}}NO
        </label>
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('enfermedad_prolongada', 'Enfermedades prolongadas o debilitantes:', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <label class="radio-inline">
            {{Form::radio('opc_enfermedad','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_enfermedad','0')}}NO
        </label>
    </span>
    {!! Form::text('enfermedad', null, ['class' => 'form-control']) !!}
    </div>    
</div>
<div class="form-group col-sm-6">
    {!! Form::label('medicado', '¿Toma algun medicamento?', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <label class="radio-inline">
            {{Form::radio('opc_medicamentos','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_medicamentos','0')}}NO
        </label>
    </span>
    {!! Form::text('medicamentos', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('alergias', '¿Presenta reacciones alergicas?', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <label class="radio-inline">
            {{Form::radio('opc_alergias','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_alergias','0')}}NO
        </label>
    </span>
    {!! Form::text('alergias', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('extraido_dientes', '¿Le han extraido piezas dentarias?:', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <label class="radio-inline">
            {{Form::radio('opc_extraido','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_extraido','0')}}NO
        </label>
    </span>
        {!! Form::text('extraido', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('dolor_sensibilidad', '¿En los ultimos dias a presentado dolor o sensibilidad en alguna pieza dentaria?', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <label class="radio-inline">
            {{Form::radio('opc_dolor','1')}}SI
        </label>
        <label class="radio-inline">
            {{Form::radio('opc_dolor','0')}}NO
        </label>
    </span>
    {!! Form::text('dolor', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group pull-right">
{!! Form::submit($submit_text) !!}
</div>
