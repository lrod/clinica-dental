<h3>Examen Facial</h3>

<table class="table">
    <tr>
        <td>Frente:</td>
        <td>{{$examenfacial->fac_frente}}</td>
    <tr>
        <td>Perfil:</td>
        <td>{{$examenfacial->fac_perfil}}</td>
    </tr>
    <tr>
        <td>Relacion labios:</td>
        <td>{{$examenfacial->fac_relacion_labios}}</td>
    <tr>
        <td>Sonrisa</td>
        <td>{{$examenfacial->fac_sonrisa}}</td>
    </tr>
    <tr>
        <td>Visualizacion de incisivos</td>
        <td>{{$examenfacial->fac_incisivos}}</td>
    <tr>
        <td>Arco de sonrisa</td>
        <td>{{$examenfacial->fac_arco_sonrisa}}</td>
    </tr>
    <tr>
        <td>Contorno gingival</td>
        <td>{{$examenfacial->fac_contorno_gingival}}</td>
     <tr>
        <td>Linea media</td>
        <td>{{$examenfacial->fac_linea_media}}</td>
    </tr>
    <tr>
        <td>Linea superior</td>
        <td>{{$examenfacial->fac_linea_sup}}</td>
    </tr>
    <tr>
        <td>Linea inferior</td>
        <td>{{$examenfacial->fac_linea_inf}}</td>
    </tr>
</table>