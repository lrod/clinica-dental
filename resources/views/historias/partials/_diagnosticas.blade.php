{!! Form::hidden('identificacion', $paciente->identificacion) !!}
{!! Form::hidden('historia_id', $paciente->historia->id) !!}
<div class="col-sm-6">
<div class="form-group"> {!! Form::label('label', 'Agregar ayuda diagnostica:', ['class' => 'control-label']) !!}
    <div class="input-group">
        {!! Form::select('tipo',[''=>'Seleccione..']+$historia->getDiagnosticas(), null, ['class' => 'form-control'] ) !!}
        {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
        {!! Form::file('archivo') !!}
        {!! Form::submit('Agregar') !!}
    </div>
</div>
</div>

