<div class="col-sm-6 ">
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-12">
        {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
    </div>
</div>
</div>
<div class="col-sm-6 ">
    <div class="form-group">
        {!! Form::label('pago', 'Pago:', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-12">
            {!! Form::number('pago', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="col-sm-12 ">
    <div class="form-group">
    {!! Form::label('trabajo_realizado', 'Trabajo realizado:', ['class' => 'control-label']) !!}

        {!! Form::textarea('trabajo_realizado', null, ['class' => 'form-control', 'rows'=>'3']) !!}

</div>
</div>
<div class="col-sm-12 ">
<div class="form-group">
    {!! Form::label('trabajo_pendiente', 'Trabajo por realizar:', ['class' => 'control-label']) !!}

        {!! Form::textarea('trabajo_pendiente', null, ['class' => 'form-control', 'rows'=>'3']) !!}

</div>
</div>
