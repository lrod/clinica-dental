{!! Form::hidden('identificacion', $paciente->identificacion) !!}
{!! Form::hidden('historia_id', $paciente->historia->id) !!}
<div class="col-sm-12 ">
    <div class="form-group">
        {!! Form::label('label', 'Agregar avance del procedimiento:', ['class' => 'control-label']) !!}        
    </div>
</div>    
<div class="col-sm-6 ">
    <div class="form-group">
        {!! Form::label('fecha', 'Fecha:', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-12">
            {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="col-sm-6 ">
    <div class="form-group">
        {!! Form::label('archivo', 'Archivo:', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-12">
            {!! Form::file('archivo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="form-group">
        {!! Form::label('descripcion', 'Descripcion:', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-12">
            {!! Form::textarea('descripcion') !!}
        </div>
    </div>
</div>
<div class="col-sm-6 ">
{!! Form::submit('Agregar') !!}
</div>   
