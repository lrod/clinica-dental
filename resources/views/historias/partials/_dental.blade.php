
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('oclusion_molar_derecha', 'Oclusion molar derecha:', ['class' => 'control-label']) !!}
    {!! Form::text('oclusion_molar_derecha', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('oclusion_molar_izquierda', 'Oclusión molar izquierda:', ['class' => 'control-label']) !!}
    {!! Form::text('oclusion_molar_izquierda', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('relacion_canina_derecha', 'Relacion canina derecha:', ['class' => 'control-label']) !!}
    {!! Form::text('relacion_canina_derecha', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('relacion_canina_izquierda', 'Relacion canina izquierda:', ['class' => 'control-label']) !!}
        {!! Form::text('relacion_canina_izquierda', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('sobremordida_vertical', 'Sobremordida vertical:', ['class' => 'control-label']) !!}
    {!! Form::text('sobremordida_vertical', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('sobremordida_horizontal', 'Sobremordida horizontal:', ['class' => 'control-label']) !!}
        {!! Form::text('sobremordida_horizontal', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('mordida_cruzada_anterior', 'Mordida cruzada anterior:', ['class' => 'control-label']) !!}
    {!! Form::text('mordida_cruzada_anterior', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('mordida_cruzada_posterior', 'Mordida cruzada posterior:', ['class' => 'control-label']) !!}
        {!! Form::text('mordida_cruzada_posterior', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
<div class="form-group">
    {!! Form::label('alteracion_erupcion_dentaria', 'Alteracin de la erupción dentaria:', ['class' => 'control-label']) !!}
    {!! Form::text('alteracion_erupcion_dentaria', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('examen_atm', 'Examen ATM:', ['class' => 'control-label']) !!}
        {!! Form::text('examen_atm', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-12>
    <div class="form-group">
        {!! Form::label('habitos', 'Habitos:', ['class' => 'control-label']) !!}
        {!! Form::text('habitos', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('examen_periodontal', 'Periodontal:', ['class' => 'control-label']) !!}
        {!! Form::textarea('examen_periodontal', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
    <div class="form-group">
        {!! Form::label('examen_radiografico', 'Radiografico:', ['class' => 'control-label']) !!}
        {!! Form::textarea('examen_radiografico', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class=col-sm-6>
{!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>