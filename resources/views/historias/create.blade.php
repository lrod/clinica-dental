@extends('layouts.app')

@section('content')
    <h3>Crear historia clinica</h3>

    {!! Form::model(new App\Historia, ['route' => ['pacientes.historias.store', $paciente->identificacion]]) !!}
    @include('procedimientos/partials/_form', ['submit_text' => 'Crear Historia'])
    {!! Form::close() !!}
@endsection