@extends('layouts.app')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">Reportes</div>
			 <div class="panel-body">
				 <section class="content"  id="contenido_reporte">
					 <table class="table table-bordered table-striped" width="100%">
						<thead>
							<tr>
								<th>Reporte</th>
								<th>Ver</th>
								<th>Descargar</th>
							</tr>
						</thead>
						<tbody>

						@foreach($reportes as $id => $reporte)
							<tr>
								<td><a href="javascript:void(0);" onclick="cargarReporte({{$id}});">{{$reporte}}</a></td>
								<td>{!! link_to_action('PdfController@crearPdf','Ver', array('tipo'=>$id,'accion'=>1,'titulo'=>$reporte), array('target'=>'_blank'))!!}</td>
								<td>{!! link_to_action('PdfController@crearPdf','Descargar', array('tipo'=>$id,'accion'=>2,'titulo'=>$reporte), array('target'=>'_blank'))!!}</td>
							</tr>
						@endforeach
						</tbody>
					 </table>
				 </section>
			</div>
		</div>

@stop()