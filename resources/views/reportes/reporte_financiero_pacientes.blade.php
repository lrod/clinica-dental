<div class="box box-primary">
    <div class="box-header">
        @if(!$isPdf)
            <div class="pull-right">
                {!! link_to('/reportes', 'Regresar al listado de reportes') !!}
            </div>
        @else
            <div class="pull-right">
                <img src="{{asset('uploads/logo.png')}}" alt="Logo" id="logo" style="width:240px;height:38px">
            </div>
        @endif
        <div class="pull-left">
            <h4 class="box-title">{{$titulo}}</h4>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered table-striped" width="100%">
            <thead>
            <tr>
                <th>Identificacion</th>
                <th>Nombres</th>
                <th>Total</th>

                <th>Abonos</th>
                <th>Saldo</th>
            </tr>
            </thead>
            <tbody>
            @foreach($datos as $dato)
            <tr>
                @php($pac = Helpers::getPacienteByIdentificacion($dato->identificacion)->first())
                <td>{{$dato->identificacion}}</td>
                <td>{{$pac->nombreCompleto()}}</td>
                <td class="valor">{{Helpers::getTotalProcedimientos($pac->procedimientos)}}</td>
                <td class="valor">{{Helpers::getTotalCuotaInicial($pac->id)}}</td>

                <td class="valor">{{Helpers::getSaldo($pac->procedimientos, $pac->controles, $pac->id)}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>