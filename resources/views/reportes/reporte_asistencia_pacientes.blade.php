@if($isPdf)
    <style>
        .table {
            margin: 1em 0;
            width: 100%;
            overflow: hidden;
            background: #FFF;
            border-radius: 5px;
            border: 1px solid #167F92;
        }
        .table-bordered {
            border-collapse: collapse;
            border: 1px solid #167F92;
        }
        .table-bordered > thead > tr > th,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > tbody > tr > td,
        .table-bordered > tfoot > tr > td {
            padding: 2px 1px 2px 4px;
            border: 1px solid #167F92;;
            border-bottom: 1px solid #e0e0e0;
        }

        .table-bordered > thead > tr > th,
        .table-bordered > thead > tr > td{
            background: #4cae4c;
            color: #ffffee;
        }
        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #f5f5f5;
        }
        .valor{
            text-align: right;
        }
        .pull-right {
            float: right !important;
        }
        .pull-left {
            float: left !important;
        }
        .box-title{
            font-size: 1.5rem;
            font-family: Sans;
            color: #285c00;
            background:#e0e0e0;
            border-radius: 3px;
            border: 1px solid #167F92;
            padding: 3px;
        }
    </style>
@endif

<div class="box box-primary">
    <div class="box-header">
        <div class="push-left">
            <h4 class="box-title">{{$titulo}}</h4>
        </div>
        <div class="pull-right">
            {!! link_to('/reportes', 'Regresar al listado de reportes') !!}
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered table-striped" width="100%">
            <thead>
            <tr>
                <th>Identificacion</th>
                <th>Nombres</th>
                <th>Ultimo control</th>
                <th>Tiempo trasnscurrido</th>
            </tr>
            </thead>
            <tbody>
            @foreach($datos as $dato)
            <tr>
                @php($pac = Helpers::getPacienteByIdentificacion($dato->identificacion)->first())
                @php($date = new \Carbon\Carbon($dato->fecha))
                @php($tiempo_transc = $date->diffInDays(\Carbon\Carbon::now(new DateTimeZone('America/Bogota'))))
                <td>{{$dato->identificacion}}</td>
                <td>{{Helpers::getNombreCompleto($pac)}}</td>
                <td>{{$dato->fecha}}</td>
                <td>{{$tiempo_transc}} dias</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>