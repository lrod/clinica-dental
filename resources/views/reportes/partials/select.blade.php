<div class=col-sm-12>

    <div class="form-group form-horizontal">
        @php($year = $date->year)
        @php($month = $date->month)

        {!! Form::label('mes', 'Periodo:', ['class' => 'control-label']) !!}
        {!! Form::select('anio', $years, $year, ['id'=>'year-stats','class' => 'form-control']) !!}
        {!! Form::select('mes', ['-1'=>'Seleccione']+$meses, $month-1, ['id'=>'month-stats','class' => 'form-control']) !!}
        {{-- Form::date('mes', 'Mes:', ['id'=>'select_mes', 'class' => 'datepicker control-label']) !! --}}
        {{-- Form::submit($submit_text, ['class'=>'btn btn-primary']) !! --}}

    </div>

</div>