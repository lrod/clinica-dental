@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Reportes</div>
        <div class="panel-body">
            <div class="col-sm-6">
                @php($meses = Helpers::getMeses())
                @php($years = Helpers::getAnios())
                {!! Form::open(['id'=>'form_reporte']) !!}
                @include('reportes.partials.select',['meses' => $meses, 'date'=> $date, 'submit_text' => 'Update'] )
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-sm-12">
            <div class="row"> <!-- Row totales -->
                <div class="col-sm-12">

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <i class="fa fa-calendar fa-2x"></i>
                                            Citas</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-right" id="info-citas">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <i class="fa fa-check-square fa-2x"></i>
                                            Controles</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-right" id="info-controles">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <i class="fa fa-medkit fa-2x"></i>
                                            Tratamientos</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-right" id="info-tratamientos">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <i class="fa fa-user fa-2x"></i>
                                            Pacientes atendidos</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-right" id="info-pacientes">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="">
                                        <i class="fa fa-book fa-2x"></i>
                                        Salidas de Inventario</div>
                                </div>
                            </div>
                            <div class="row">
                                <table class="table table-stripped table-bordered">
                                    <thead>
                                    <th>ID</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    </thead>
                                    <tbody id="info-inventario-out">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>	<!-- /Row info extra -->
            <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div id="citas_stats_div">
                        {!! $lava->render('ColumnChart', 'CitasMensuales', 'citas_stats_div') !!}
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div id="controles_stats_div">
                            {!! $lava->render('ColumnChart', 'ControlesMensuales', 'controles_stats_div') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div id="ingresos_stats_div">
                                {!! $lava->render('ColumnChart', 'IngresosMensuales', 'ingresos_stats_div') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div id="gastos_stats_div">
                                {!! $lava->render('ColumnChart', 'GastosMensuales', 'gastos_stats_div') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@stop()