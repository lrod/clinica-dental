@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Agenda</div>

        <div class="panel-body">
            <div class="row">

                <div class="col-sm-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Citas para hoy: {{\Carbon\Carbon::now(new DateTimeZone('America/Bogota'))->formatLocalized('%A %d %B %Y')}}</div>
                    <div class="panel-body">
                        <div>
                        @if(count($citas)==0)
                            <span>No hay citas pendiente para hoy</span>
                        @else
                            <ul class="list-group">
                            @foreach($citas as $cita)
                                <li class="list-group-item">
                                    Hora: <span>{{$cita->hora}}</span><br>
                                    Paciente:<a href="{{ route('pacientes.show', $cita->paciente->identificacion)}}"> <span>{{$cita->paciente->nombreCompleto()}}</span></a><br>
                                    Medico: <span>{{$cita->medico->nombreCompleto()}}</span>
                                </li>
                            @endforeach
                            </ul>
                        @endif
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-sm-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Calendario</div>
                        <div class="panel-body">
                            <div id="fullcalendario"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
