<div class="form-group col-sm-6">
    {!! Form::label('tipo_identificacion', 'Tipo identificacion:', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_identificacion', array('CC'=>'Cedula'), null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('identificacion', 'Identificacion:', ['class' => 'control-label']) !!}
    {!! Form::number('identificacion', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('nombre1', 'Primer nombre:', ['class' => 'control-label']) !!}
    {!! Form::text('nombre1', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('nombre2', 'Segundo nombre:', ['class' => 'control-label']) !!}
    {!! Form::text('nombre2', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('apellido1', 'Primer apellido:', ['class' => 'control-label']) !!}
    {!! Form::text('apellido1',  null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('apellido2', 'Segundo apellido:', ['class' => 'control-label']) !!}
    {!! Form::text('apellido2',  null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('telefono1', 'Telefono:', ['class' => 'control-label']) !!}
    {!! Form::text('telefono1', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('direccion', 'Direccion:', ['class' => 'control-label']) !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('especialidad', 'Especialidad:', ['class' => 'control-label']) !!}
    {!! Form::text('especialidad', null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
