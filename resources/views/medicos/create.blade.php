@extends('layouts.app')

@section('content')

        <div class="panel panel-default">
        <div class="panel-heading">Crear medico</div>
        <div class="panel-body">

        {!! Form::model(new App\Medico, ['route' => ['medicos.store']]) !!}
            @include('medicos/partials/_form', ['submit_text' => 'Crear Medico'])
        {!! Form::close() !!}

            </div>
        </div>

@stop