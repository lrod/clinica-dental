@extends('layouts.app')

@section('content')


            <div class="panel panel-default">
                <div class="panel-heading ">
                		<span class="fa fa-user-md fa-fw"></span>
                		 Informacion del medico
					<div class="pull-right">
						<span class="glyphicon glyphicon-arrow-left"></span>
        				{!! link_to_route('medicos.index', 'Regresar al listado de medicos') !!} 
      		    	</div>
                </div>

                <div class="panel-body"> 

                @if($medico !== null)
				<div class="">
					<i></i>
				</div>
				<div class="pull-left">
					<h4>{{ $medico->identificacion }}</h4>
					<h3>{{ $medico->nombreCompleto() }}</h3>
					<h4>{{ $medico->especialidad }}</h4>
				</div>
				<div class=" pull-right">
					<div>
						@permission('medico-edit')
						{!! link_to_route('medicos.edit', 'Editar', array($medico->id), array('class' => 'btn btn-info')) !!}
						@endpermission
						@permission('medico-delete')
						{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('medicos.destroy', $medico->id))) !!}
						{!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
						{!! Form::close() !!}
						@endpermission
					</div>
					<div>
						@php($canEnable = Entrust::can('medico-enable'))
						@if($canEnable)
							Estado: {!! link_to_action('MedicoController@habilitar', $medico->habilitado?'Habilitado':'Deshabilitado', array($medico->id), null) !!}
						@else
							Estado: {{$medico->habilitado?'Habilitado':'Deshabilitado'}}
						@endif
					</div>

				</div>
				</div>
				<table class="table table-striped" width="100%">
					
					<tr>
						<td>Dirección:</td>
						<td>{{ $medico->direccion }}</td>
					</tr>
					<tr>
						<td>Telefono:</td>
						<td>{{ $medico->telefono1 }}</td>
					</tr>
					<tr>
						<td>Correo:</td>
						<td>{{ $medico->email }}</td>						
					</tr>					
				</table>
				</div>
				

				@endif

@stop

