@extends('layouts.app')

@section('content')

		<div class="panel panel-default">
			<div class="panel-heading">Medicos</div>

			<div class="panel-body">
				<div class="pull-right">
				@permission('medico-create')
				<p> {!! link_to_route('medicos.create', 'Crear Medico', null, array('class' => 'btn btn-success')) !!}</p>
				@endpermission
				</div>
				<table class="table table-striped">
					<thead>
					<th>Identificacion</th>
					<th>Nombre</th>
					<th>Especialidad</th>
					<th>Estado</th>
					<th>Acciones</th>
				</thead>
				<tbody>
					@foreach($medicos as $medico)
						<tr>
						<td>{{$medico->identificacion}}</td>
						<td><a href="{{ route('medicos.show', $medico->id,['tab'=>'']) }}">{{ $medico->nombreCompleto() }}</a></td>
						<td>{{$medico->especialidad}}</td>
						<td>{{$medico->habilitado?'Habilitado':'Deshabilitado'}}</td>
						@permission('medico-edit')
						<td> {!! link_to_route('medicos.edit', 'Editar', array($medico->id), array('class' => 'btn btn-default')) !!} </td>
						@endpermission
						</tr>
					@endforeach
				</tbody>
				</table>
			</div>
		</div>

@stop