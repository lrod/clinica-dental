@extends('layouts.app')

@section('content')

            <div class="panel panel-default">
                <div class="panel-heading">Editar medico</div>

                <div class="panel-body">

			    {!! Form::model($medico, ['method' => 'PATCH', 'route' => ['medicos.update', $medico->id]]) !!}
			        @include('medicos/partials/_form', ['submit_text' => 'Editar medico'])
			    {!! Form::close() !!}

			    </div>
            </div>

@endsection
